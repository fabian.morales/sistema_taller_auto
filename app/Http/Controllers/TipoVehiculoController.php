<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use App\Http\Controllers\AdminController;
use App\Models\TipoVehiculo;

class TipoVehiculoController extends AdminController {

    public function mostrarIndex(){
        $tipos = TipoVehiculo::paginate(20);
        return View::make('tipo_vehiculo.index', array("tipos" => $tipos));
    }
    
    public function mostrarFormTipo($tipo){
        if (!sizeof($tipo)){
            $tipo = new TipoVehiculo();
        }
        
        $partes = \App\Models\ParteVehiculo::with(array("tipos" => function($q) use ($tipo) {
            $q->where("id_tipo", $tipo->id);
        }))->get();
        
        $imgFile = public_path('img/mapas/tipo_'.$tipo->id.'.jpg');
        $img = '';
        if (is_file($imgFile)){
            $img = 'img/mapas/tipo_'.$tipo->id.'.jpg';
        }
        
        return View::make("tipo_vehiculo.form", array("tipo" => $tipo, "partes" => $partes, "img" => $img));
    }
    
    public function crearTipo(){
        return $this->mostrarFormTipo(new TipoVehiculo());
    }
    
    public function editarTipo($id){
        $tipo = TipoVehiculo::find($id);
        if (!sizeof($tipo)){
            return Redirect::action('TipoVehiculoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el tipo de vehiculo");
        }
        
        return $this->mostrarFormTipo($tipo);
    }
    
    public function guardarTipo(){
        $id = Input::get("id");

        $tipo = TipoVehiculo::find($id);
        if (!sizeof($tipo)){
            $tipo = new TipoVehiculo();
        }

        $tipo->fill(Input::all());
        
        if ($tipo->save()){
            
            $partes = array();
            $partesTxt = Input::get("partes");
            $partesCheck = Input::get("assoc_partes");
            
            if (sizeof($partesCheck)){
                foreach ($partesCheck as $k => $p){
                    $path = $partesTxt[$k];
                    if (!empty($path)){
                        $partes[$k] = array("path" => $path);
                    }
                }
            }
            
            $tipo->partes()->sync($partes, false);
            
            if (Input::hasFile('imagen') && Input::file('imagen')->isValid()) {
                Input::file('imagen')->move(public_path('img/mapas'), 'tipo_'.$tipo->id.'.jpg');
            }
            
            return Redirect::action('TipoVehiculoController@mostrarIndex')->with("mensaje", "Tipo de vehículo guardado exitosamente");
        }
        else{
            return Redirect::action('TipoVehiculoController@mostrarIndex')->with("mensajeError", "No se pudo guardar el tipo de vehículo");
        }
    }
}