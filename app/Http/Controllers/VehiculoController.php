<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use App\Http\Controllers\AdminController;
use App\Models\Vehiculo;
use App\Models\Cliente;

class VehiculoController extends AdminController {

    public function mostrarIndex(){
        $vehiculos = Vehiculo::with("propietario")->paginate(20);
        return View::make('vehiculo.index', array("vehiculos" => $vehiculos));
    }
    
    public function mostrarListaVehiculos(){
        $vehiculos = Vehiculo::with("propietario");
        
        $placa = Input::get("placa");
        $cedula = Input::get("cedula");
        
        if (!empty($placa)){
            $vehiculos = $vehiculos->where("placa", $placa);
        }
        
        if (!empty($cedula)){
            $vehiculos= $vehiculos->whereHas("propietario", function($q) use ($cedula) {
                $q->where("id_nit", $cedula);
            });
        }
        
        $vehiculos = $vehiculos->paginate(20);
        return View::make('vehiculo.lista', array("vehiculos" => $vehiculos));
    }
    
    public function mostrarFormVehiculo($vehiculo){
        if (!sizeof($vehiculo)){
            $vehiculo = new Vehiculo();
        }
        
        $tipos = \App\Models\TipoVehiculo::all();
        return View::make("vehiculo.form", array("vehiculo" => $vehiculo, "tipos" => $tipos));
    }
    
    public function crearVehiculo(){
        return $this->mostrarFormVehiculo(new \App\Models\Vehiculo());
    }
    
    public function editarVehiculo($id){
        $vehiculo = Vehiculo::where("id", $id)->with(array(
            "propietario", 
            "documentos" => function($q) {
                $q->orderBy("fecha", "desc");
            }, 
            "documentos.tipoDocumento"))->first();
        if (!sizeof($vehiculo)){
            return Redirect::action('VehiculoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el vehiculo");
        }
        
        return $this->mostrarFormVehiculo($vehiculo);
    }
    
    public function guardarVehiculo(){
        $id = Input::get("id");
        $cedula = Input::get("cedula_propietario");
        $nombre = Input::get("nombre_propietario");
        $apellido = Input::get("apellido_propietario");
        
        $cliente = Cliente::where("id_nit", $cedula)->first();
        if (!sizeof($cliente)){
            $cliente = new Cliente();
            $cliente->id_nit = $cedula;
        }

        $cliente->nombres = $nombre;
        $cliente->apellidos = $apellido;

        if (!$cliente->save()){
            throw new Exception('No se pudo guardar los datos del cliente');
        }

        $vehiculo = Vehiculo::find($id);
        if (!sizeof($vehiculo)){
            $vehiculo = new Vehiculo();
        }

        $vehiculo->fill(Input::all());
        $vehiculo->id_propietario = $cliente->id;
        
        if ($vehiculo->save()){
            return Redirect::action('VehiculoController@mostrarIndex')->with("mensaje", "Vehículo guardado exitosamente");
        }
        else{
            return Redirect::action('VehiculoController@mostrarIndex')->with("mensajeError", "No se pudo guardar el vehículo");
        }
    }
}