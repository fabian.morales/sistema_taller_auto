<?php

namespace App\Http\Controllers;

use View;
use Input;
use Auth;
use Redirect;
use DB;
use Exception;
use App\Http\Controllers\AdminController;

class OrdenServicioController extends AdminController {

    public function mostrarIndex() {
        $ordenes = \App\Models\Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
            $query->where('tipo_mov', 'S');
        })->orderBy('created_at', 'desc')->paginate(20);

        return View::make('ordenservicio.index', array("ordenes" => $ordenes));
    }
    
    public function obtenerListaDocs(){
        $fechaInicio = !empty(Input::get("fecha_inicio")) ? Input::get("fecha_inicio") : '2000-01-01';
        $fechaFin = !empty(Input::get("fecha_fin")) ? Input::get("fecha_fin") : '2100-12-31';
        $cedula = Input::get("cedula");
        $placa = Input::get("placa");
        
        $ordenes = \App\Models\Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
            $query->where('tipo_mov', 'S');
        })->where('fecha', '>=', $fechaInicio)->where('fecha', '<=', $fechaFin);
        
        if (!empty($cedula)){
            $ordenes = $ordenes->whereHas("cliente", function($q) use ($cedula) {
                $q->where("id_nit", $cedula);
            });
        }
        
        if (!empty($placa)){
            $ordenes = $ordenes->whereHas("vehiculo", function($q) use ($placa) {
                $q->where("placa", $placa);
            });
        }
        
        $ordenes = $ordenes->orderBy('created_at', 'desc')->paginate(20);
        
        return View::make('ordenservicio.lista', array("ordenes" => $ordenes));
    }

    public function verDetalle($id) {
        $documento = \App\Models\Documento::with(array("docDiagnostico.imagenes", "docDiagnostico.partes.parteVehiculo", "docDiagnostico.productos.producto", "usuarioCreacion", "cliente", "tipoDocumento", "vehiculo.tipo"))->where("id", $id)->first();

        if (!sizeof($documento)) {
            return Redirect::action('OrdenEntradaController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        } 
        else {
            $opciones = \App\Models\OpcionDg::all();

            $categorias = \App\Models\CategoriaDg::with(array(
                "items" => function ($q) use ($documento) {
                    $q->whereHas("tipos", function($q) use ($documento) {
                        $q->where("id_tipo", $documento->id_tipo);
                    });
                },
                "items.tipos" => function($q) use ($documento) {
                    $q->where("id_tipo", $documento->id_tipo);
                },
                "items.diagnosticos" => function($q) use ($documento) {
                    $q->where("id_doc_dg", $documento->docDiagnostico->first()->id);
                }
            ))->whereHas("items.tipos", function($q) use ($documento) {
                $q->where("id_tipo", $documento->id_tipo);
            })->get();

            return View::make("ordenservicio.detalle", array("documento" => $documento, "categorias" => $categorias, "opciones" => $opciones));
        }
    }

    public function mostrarFormOrden($documento) {
        if (!sizeof($documento)) {
            $documento = new \App\Models\Documento();
        }

        $tipoDoc = \App\Models\TipoDocumento::where("tipo_mov", "S")->first();

        $categorias = \App\Models\CategoriaDg::with(array("items" => function ($q) use ($tipoDoc) {
                $q->whereHas("tipos", function($q) use ($tipoDoc) {
                    $q->where("id_tipo", $tipoDoc->id);
                });
            },
            "items.diagnosticos" => function($q) use ($documento) {
                if (sizeof($documento->docDiagnostico)){
                    $q->where("id_doc_dg", $documento->docDiagnostico[0]->id);
                }
                else{
                    $q->where("id_doc_dg", 0);
                }
            },
            "items.tipos" => function($q) use ($tipoDoc) {
                $q->where("id_tipo", $tipoDoc->id);
            }))->whereHas("items.tipos", function($q) use ($tipoDoc) {
            $q->where("id_tipo", $tipoDoc->id);
        })->get();

        $opciones = \App\Models\OpcionDg::all();
        $tipos = \App\Models\TipoDocumento::where("tipo_mov", "S")->get();
        $productos = \App\Models\Producto::where("tipo", "P")->orWhere("tipo", "S")->get();

        return View::make("ordenservicio.form", array("documento" => $documento, "categorias" => $categorias, "opciones" => $opciones, "tipos" => $tipos, "productos" => $productos));
    }

    public function crearOrden() {
        return $this->mostrarFormOrden(new \App\Models\Documento());
    }
    
    public function editarOrden($id){
        $documento = \App\Models\Documento::with(array("docDiagnostico.productos.producto", "usuarioCreacion", "cliente.vehiculos", "tipoDocumento", "vehiculo.tipo"))->where("id", $id)->first();
        if (!sizeof($documento)){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        
        if (sizeof($documento->docDiagnostico) && $documento->docDiagnostico[0]->estado == "C"){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensaje", "Este documento ya se encuentra cerrado");
        }
        
        return $this->mostrarFormOrden($documento);
    }

    public function guardarOrden() {
        try {
            DB::beginTransaction();

            $id = Input::get("id");
            $documento = \App\Models\Documento::find($id);            
            if (!sizeof($documento)){
                $documento = new \App\Models\Documento();
                $documento->fecha = date('Y-m-d H:i:s');
            }
            
            $idDocDg = Input::get("id_docdg");
            $docDg = \App\Models\DocumentoDg::find($idDocDg);
            if (!sizeof($docDg)){
                $docDg = new \App\Models\DocumentoDg();    
            }
            
            if ($docDg->estado == "C"){
                throw new Exception('Este documento ya se encuentra cerrado');
            }
            
            $idTipoDoc = Input::get("id_tipo");
            $tipoDoc = \App\Models\TipoDocumento::find($idTipoDoc);

            $cedula = Input::get("cedula");
            $nombre = Input::get("nombre");
            $apellidos = Input::get("apellidos");
            $telefono = Input::get("telefono");
            $direccion = Input::get("direccion");

            $cliente = \App\Models\Cliente::where("id_nit", $cedula)->first();
            if (!sizeof($cliente)) {
                $cliente = new \App\Models\Cliente();
                $cliente->id_nit = $cedula;
            }

            $cliente->nombres = $nombre;
            $cliente->apellidos = $apellidos;
            $cliente->telefono = $telefono;
            $cliente->direccion = $direccion;

            if (!$cliente->save()) {
                throw new Exception('No se pudo guardar los datos del cliente');
            }

            $idVehiculo = Input::get("id_vehiculo");
            $vehiculo = \App\Models\Vehiculo::find($idVehiculo);

            if (!sizeof($vehiculo)) {
                throw new Exception('El veh&iacute;culo ingresado no existe');
            }

            if ($vehiculo->id_propietario !== $cliente->id) {
                throw new Exception('El veh&iacute;culo ingresado no pertenece al cliente');
            }

            $vehiculo->marca = Input::get("marca");
            $vehiculo->motor = Input::get("motor");
            $vehiculo->serie = Input::get("serie");
            $vehiculo->color = Input::get("color");
            $vehiculo->placa = Input::get("placa");
            $vehiculo->km = Input::get("km");
            $vehiculo->cilindraje = Input::get("cilindraje");
            $vehiculo->caja = Input::get("caja");

            if (!$vehiculo->save()) {
                throw new Exception('No se pudo actualizar los datos del veh&iacute;culo');
            }

            $documento->id_tipo = $idTipoDoc;
            $documento->fecha = date('Y-m-d H:i:s');
            $documento->id_cliente = $cliente->id;
            $documento->observaciones = Input::get("observaciones");
            $documento->id_usuario_crea = Auth::user()->id;
            $documento->id_vehiculo = $vehiculo->id;

            if (!$documento->save()) {
                throw new Exception('No se pudo guardar el documento');
            }

            $docDg->id_documento = $documento->id;
            $docDg->km = $vehiculo->km;
            $docDg->motor = $vehiculo->motor;
            $docDg->color = $vehiculo->color;
            $docDg->caja = $vehiculo->caja;
            $docDg->cilindraje = $vehiculo->cilindraje;
            $docDg->estado_general = Input::get('estado_general');
            $docDg->recomendaciones = Input::get('recomendaciones');
            
            $estado = Input::get("estado");           
            $docDg->estado = $estado == "C" ? "C" : "N";

            if (!$docDg->save()) {
                throw new Exception('No se pudo guardar los datos del diagn&oacute;stico');
            }

            $items = Input::get("resp");
            $cantidades = Input::get("cant_item");
            $observaciones = Input::get("observaciones_item");

            if (sizeof($items)){
                foreach ($items as $i => $opcion) {
                    $dg = \App\Models\Diagnostico::where("id_doc_dg", $docDg->id)->where("id_item_dg", $i)->first();
                    if(!sizeof($dg)){
                        $dg = new \App\Models\Diagnostico();
                        $dg->id_doc_dg = $docDg->id;
                        $dg->id_item_dg = $i;
                    }
                    
                    $dg->id_opc_dg = $opcion;
                    $dg->cantidad = (sizeof($cantidades)) && array_key_exists($i, $cantidades) ? (int) $cantidades[$i] : 0;
                    $dg->observaciones = $observaciones[$i];

                    if (!$dg->save()) {
                        throw new Exception('No se pudo guardar un detalle del diagn&oacute;stico');
                    }
                }
            }

            $productos = Input::get("id_producto");
            $cantidades = Input::get("cantidad");
            $valoresUnit = Input::get("valor_unitario");
            $valoresTotal = Input::get("valor_total");

            if (sizeof($productos)){
                foreach ($productos as $k => $p){
                    $cantidad = $cantidades[$k];
                    if ($cantidad > 0 && !empty($p)){
                        $producto = \App\Models\Producto::find($p);

                        if (!sizeof($producto)){
                            throw new Exception('El producto '.$p.' no existe');
                        }

                        if ($producto->tipo != "P" && $producto->tipo != "S"){
                            throw new Exception('El tipo del producto '.$p.' - '.$producto->nombre.' no es válido');
                        }

                        if ((int)$valoresUnit[$k] <= 0){
                            throw new Exception('El producto '.$p.' - '.$producto->nombre.' no tiene precio');
                        }

                        $movimiento = \App\Models\ProductoDg::where("id_doc_dg", $docDg->id)->where("id_producto", $p)->first();
                        if(!sizeof($movimiento)){
                            $movimiento = new \App\Models\ProductoDg();
                            $movimiento->id_doc_dg = $docDg->id;
                            $movimiento->id_producto = $p;
                        }
                        
                        $movimiento->cantidad = $cantidad;
                        $movimiento->valor_unitario = $valoresUnit[$k];                    
                        $movimiento->valor_total = $valoresTotal[$k];

                        if (!$movimiento->save()){
                            throw new Exception('No se pudo guardar el registro del item '.$p.' - '.$producto->nombre.' en el documento');
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return $this->retornarError($e);
        }

        return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensaje", "Documento guardado exitosamente");
    }

    public function buscarVehiculo($id) {
        $vehiculo = \App\Models\Vehiculo::where("id", $id)->with("tipo.partes")->first();
        if (!sizeof($vehiculo)) {
            $vehiculo = new \App\Models\Vehiculo();
        }

        return $vehiculo->toJson();
    }

    public function imprimirPdf($id) {
        $documento = \App\Models\Documento::with(array("docDiagnostico.imagenes", "docDiagnostico.partes.parteVehiculo", "docDiagnostico.productos.producto", "usuarioCreacion", "cliente", "tipoDocumento", "vehiculo.tipo"))->where("id", $id)->first();

        if (!sizeof($documento)) {
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        } 
        else {
            $opciones = \App\Models\OpcionDg::all();

            $categorias = \App\Models\CategoriaDg::with(array(
                "items" => function ($q) use ($documento) {
                    $q->whereHas("tipos", function($q) use ($documento) {
                        $q->where("id_tipo", $documento->id_tipo);
                    });
                },
                "items.tipos" => function($q) use ($documento) {
                    $q->where("id_tipo", $documento->id_tipo);
                },
                "items.diagnosticos" => function($q) use ($documento) {
                    $q->where("id_doc_dg", $documento->docDiagnostico->first()->id);
                }
            ))->whereHas("items.tipos", function($q) use ($documento) {
                $q->where("id_tipo", $documento->id_tipo);
            })->get();

            $html = View::make("ordenservicio.impresionPdf", array("documento" => $documento, "categorias" => $categorias, "opciones" => $opciones))->render();
            $pdf = new \App\Lib\myPdf();
            $pdf->render($html, "Documento_" . $documento->tipoDocumento->sigla . "_" . $documento->id);
        }
    }
    
    public function generarFactura($id){
        $documento = \App\Models\Documento::with(array("docDiagnostico.productos.producto", "cliente", "tipoDocumento", "docGenerado"))->where("id", $id)->first();
        
        if (!sizeof($documento)) {
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        
        if (sizeof($documento->docGenerado)){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Ya se ha generado la cuenta de cobro para esta documento");
        }
        
        $jsonDocumento = array(
            "id_vehiculo" => $documento->id_vehiculo,
            "cedulaCliente" => $documento->cliente->id_nit,
            "detalle" => array(),
            "valorDescuento" => 0,
            "id_doc_rel" => $id
        );
        
        $cantidad = 0;
        $valorTotal = 0;
        
        foreach ($documento->docDiagnostico->first()->productos as $p){
            $jsonDocumento["detalle"][] = array(
                "idProducto" => $p->id_producto,
                "cantidad" => $p->cantidad,
                "valorUnitario" => $p->valor_unitario,
                "descuento" => 0,
                "valorDescuento" => 0,
                "total" => $p->valor_total
            );
            
            $cantidad += $p->cantidad;
            $valorTotal += $p->valor_total;
        }
        
        $jsonDocumento["cantidad"] = $cantidad;
        $jsonDocumento["total"] = $valorTotal;
        
        //return json_encode($jsonDocumento);
        \Session::put('facturacion.documento', json_encode($jsonDocumento));
        return Redirect::action('FacturacionController@crearFactura');
    }
    
    public function verListaPrestamos($id){
        $documento = \App\Models\Documento::with(array("tipoDocumento", "docDiagnostico.prestamos.empleado"))->where("id", $id)->first();
        
        if (!sizeof($documento)){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        
        return View::make("ordenservicio.listaPrestamos", array("documento" => $documento));
    }
    
    public function mostrarFormPrestamo($id){
        $documento = \App\Models\Documento::with(array("docDiagnostico"))->where("id", $id)->first();
        
        if (!sizeof($documento)){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        
        $empleados = \App\User::all();
        
        return View::make("ordenservicio.formPrestamo", array("documento" => $documento, "empleados" => $empleados));
    }
    
    public function guardarPrestamo(){
        $idOrden = Input::get("id_orden");
        $idEmpleado = Input::get("id_empleado");
        $valor = (float)Input::get("valor");

        $orden = \App\Models\DocumentoDg::find($idOrden);
        if (!sizeof($orden)){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        
        $empleado = \App\User::find($idEmpleado);
        if (!sizeof($empleado)){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Empleado no encontrado");
        }
        
        if ($valor === 0){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "Debe ingresar el valor");
        }
        
        $prestamo = new \App\Models\Prestamo();
        $prestamo->id_orden = $idOrden;
        $prestamo->id_empleado = $idEmpleado;
        $prestamo->valor = $valor;
        $prestamo->fecha = date('Y-m-d H:i:s');
        
        if ($prestamo->save()){
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensaje", "Préstamo guardado exitosamente");
        }
        else{
            return Redirect::action('OrdenServicioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el préstamo");
        }
    }

}