<?php

namespace App\Http\Controllers;

use View;
use Input;
use Auth;
use Redirect;
use App\Http\Controllers\Controller;

class SesionController extends Controller {

    public function mostrarIndex(){
        return View::make('sesion.login');
    }
       
    public function hacerLogin(){
        $login = Input::get("login");
        $clave = Input::get("password");
        if (Auth::attempt(array('login' => $login, 'password' => $clave))){        
            return Redirect::to("/")->with("mensaje", "Logueado");
        }
        else{
            return Redirect::to("/sesion/formLogin")->with("mensajeError", "Correo o clave incorrecta");
        }
    }
    
    public function hacerLogout(){
        Auth::logout();
        return Redirect::to("/sesion/formLogin");
    }
}