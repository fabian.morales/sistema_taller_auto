<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use App\Models\Producto;
use App\Models\ServicioCargo;
use App\Models\Cargo;
use App\Http\Controllers\AdminController;

class ProductoController extends AdminController {

    public function mostrarIndex(){       
        $productos = Producto::paginate(20);
        return View::make('producto.index', array("productos" => $productos));
    }
    
    public function obtenerListaProductos(){
        $nombre = Input::get("nombre");
        
        $productos = Producto::orderBy('id');
        
        if (!empty($nombre)){
            $productos = $productos->where("nombre", "like", "%".$nombre."%");
        }
        
        $productos = $productos->paginate(20);
        $productos->setPath('productos');
        
        return View::make('producto.lista', array("productos" => $productos));
    }
    
    public function mostrarFormProducto($producto){       
        if (!sizeof($producto)){
            $producto = new Producto();
        }
        
        return View::make("producto.form", array("producto" => $producto));
    }
    
    public function crearProducto(){       
        return $this->mostrarFormProducto(new Producto());
    }
    
    public function editarProducto($id){        
        $producto = Producto::where("id", $id)->with("encargados.cargo")->first();
        if (!sizeof($producto)){
            return Redirect::action('ProductoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el producto");
        }
        
        return $this->mostrarFormProducto($producto);
    }
    
    public function guardarProducto(){        
        $id = Input::get("id");
        
        $producto = Producto::find($id);
        if (!sizeof($producto)){
            $producto = new Producto();
        }        
        
        $producto->fill(Input::all());
                
        if ($producto->save()){
            return Redirect::action('ProductoController@mostrarIndex')->with("mensaje", "Producto guardado exitosamente");
        }
        else{
            return Redirect::action('ProductoController@mostrarIndex')->with("mensajeError", "No se pudo guardar el producto");
        }
    }
    
    public function borrarProducto($id){
        $producto = Producto::find($id);
        
        if (!sizeof($producto)){
            return Redirect::action('ProductoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el producto");
        }
        
        $producto->encargados()->delete();        
        
        if ($producto->delete()){
            return Redirect::action('ProductoController@mostrarIndex')->with("mensaje", "Producto eliminado exitosamente");
        }
        else{
            return Redirect::action('ProductoController@mostrarIndex')->with("mensajeError", "No se pudo eliminar el producto");
        }
    }
    
    public function verExistencias(){       
        $productos = Producto::where("tipo", "P")->orWhere("tipo", "H")->orderBy("existencias")->paginate(20);
        return View::make('producto.existencias', array("productos" => $productos));
    }
    
    public function crearEncargado($idProducto){
        $producto = Producto::find($idProducto);
        if (!sizeof($producto)){
            return Redirect::action('ProductoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el producto");
        }
        
        return $this->mostrarFormEncargado(new ServicioCargo(), $producto);
    }
    
    public function editarEncargado($id){
        $encargado = ServicioCargo::find($id);
        
        if (!sizeof($encargado)){
            return Redirect::action('ProductoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el cargo asignado");
        }
        
        return $this->mostrarFormEncargado($encargado);
    }
    
    public function mostrarFormEncargado($encargado, $producto=null){       
        if (!sizeof($encargado)){
            $encargado = new ServicioCargo();
        }
        
        $cargos = Cargo::orderBy("nombre")->get();
        return View::make("producto.formCargo", array("encargado" => $encargado, "producto" => $producto, "cargos" => $cargos));
    }
    
    public function guardarEncargado(){
        $id = Input::get("id");
        $encargado = null;
        $idProducto = Input::get("id_producto");
        
        if (!empty($id)){
            $encargado = ServicioCargo::find($id);
        }
        else{            
            $idCargo = Input::get("id_cargo");
            $encargado = ServicioCargo::where("id_producto", $idProducto)->where("id_cargo", $idCargo)->first();
        }
        
        if (!sizeof($encargado)){
            $encargado = new ServicioCargo();
        }
        
        $encargado->fill(Input::all());
        
        if ($encargado->save()){
            return Redirect::action('ProductoController@editarProducto', array($idProducto))->with("mensaje", "Se ha asignado el cargo al producto"); 
        }
        else{
            return Redirect::action('ProductoController@editarProducto', array($idProducto))->with("mensajeError", "No se pudo asignar el cargo");
        }
    }
    
    public function borrarEncargado($id){
        //$id = Input::get("id");
        $encargado = ServicioCargo::find($id);
        if (!sizeof($encargado)){
            return Redirect::action('ProductoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar la asignación del cargo");
        }
        
        $idProducto = $encargado->id_producto;

        if ($encargado->delete()){
            return Redirect::action('ProductoController@editarProducto', array($idProducto))->with("mensaje", "Se ha quitado la asignación del cargo en el producto"); 
        }
        else{
            return Redirect::action('ProductoController@editarProducto', array($idProducto))->with("mensajeError", "No se pudo quitar la asignación el cargo");
        }
    }    
}