<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MovInvController
 *
 * @author Fabian
 */
 
namespace App\Http\Controllers;

use App\Http\Controllers\AdminController;

class CotizacionController extends AdminController {

    public function mostrarIndex(){
        $cotizaciones = \App\Models\Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
            $query->where('tipo_mov', 'C');
        })->orderBy('created_at', 'desc')->paginate(20);
        
        return \View::make('cotizacion.index', array("cotizaciones" => $cotizaciones));
    }
    
    public function obtenerListaDocs(){
        $fechaInicio = !empty(\Input::get("fecha_inicio")) ? Input::get("fecha_inicio") : '2000-01-01';
        $fechaFin = !empty(\Input::get("fecha_fin")) ? Input::get("fecha_fin") : '2100-12-31';
        $cedula = \Input::get("cedula");
        $placa = \Input::get("placa");
        
        $cotizaciones = \App\Models\Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
            $query->where('tipo_mov', 'C');
        })->where('fecha', '>=', $fechaInicio)->where('fecha', '<=', $fechaFin);
        
        if (!empty($cedula)){
            $cotizaciones = $cotizaciones->whereHas("cliente", function($q) use ($cedula) {
                $q->where("id_nit", $cedula);
            });
        }
        
        if (!empty($placa)){
            $cotizaciones = $cotizaciones->whereHas("vehiculo", function($q) use ($placa) {
                $q->where("placa", $placa);
            });
        }
        
        $cotizaciones = $cotizaciones->orderBy('created_at', 'desc')->paginate(20);
        
        return \View::make('cotizacion.lista', array("cotizaciones" => $cotizaciones));
    }
    
    public function mostrarFormCotizacion($documento){
        if (!sizeof($documento)){
            $documento = new \App\Models\Documento();
        }
        
        $tipos = \App\Models\TipoDocumento::where("tipo_mov", "C")->get();
        $productos = \App\Models\Producto::where("tipo", "P")->orWhere("tipo", "S")->get();
        $clientes = \App\Models\Cliente::all();
        $medios = \App\Models\MedioPago::all();
        
        return \View::make("cotizacion.form", array("documento" => $documento, "tipos" => $tipos, "productos" => $productos, "clientes" => $clientes, "medios" => $medios));
    }
    
    public function crearCotizacion(){       
        return $this->mostrarFormCotizacion(new \App\Models\Documento());
    }
    
    public function guardarSesion(){
        $json = \Response::json(\Input::get("json"));
        \Session::put('cotizacion.documento', $json);
    }
    
    public function obtenerSesion(){
        return \Session::get('cotizacion.documento');
    }
        
    public function guardarCotizacion(){
        try {
            \DB::beginTransaction();
            
            $documento = new \App\Models\Documento();
            $idTipoDoc = \Input::get("id_tipo");
            $tipoDoc = \App\Models\TipoDocumento::find($idTipoDoc);
            
            $cedula = \Input::get("cedula");
            $nombre = \Input::get("nombre");
            $apellidos = \Input::get("apellidos");
            $telefono = \Input::get("telefono");
            $direccion = \Input::get("direccion");

            $cliente = \App\Models\Cliente::where("id_nit", $cedula)->first();
            if (!sizeof($cliente)){
                $cliente = new \App\Models\Cliente();
                $cliente->id_nit = $cedula;
            }

            $cliente->nombres = $nombre;
            $cliente->apellidos = $apellidos;
            $cliente->telefono = $telefono;
            $cliente->direccion = $direccion;

            if (!$cliente->save()){
                throw new \Exception('No se pudo guardar los datos del cliente');
            }
            
            $idVehiculo = \Input::get("id_vehiculo");
            $vehiculo = \App\Models\Vehiculo::find($idVehiculo);

            if (!sizeof($vehiculo)) {
                throw new Exception('El veh&iacute;culo ingresado no existe');
            }

            if ($vehiculo->id_propietario !== $cliente->id) {
                throw new Exception('El veh&iacute;culo ingresado no pertenece al cliente');
            }

            $vehiculo->marca = \Input::get("marca");
            $vehiculo->motor = \Input::get("motor");
            $vehiculo->serie = \Input::get("serie");
            $vehiculo->color = \Input::get("color");
            $vehiculo->placa = \Input::get("placa");
            $vehiculo->km = \Input::get("km");
            $vehiculo->cilindraje = \Input::get("cilindraje");
            $vehiculo->caja = \Input::get("caja");

            if (!$vehiculo->save()) {
                throw new Exception('No se pudo actualizar los datos del veh&iacute;culo');
            }

            $documento->id_tipo = $idTipoDoc;
            $documento->fecha = date('Y-m-d H:i:s');
            $documento->id_cliente = $cliente->id;
            $documento->observaciones = \Input::get("observaciones");
            $documento->id_usuario_crea = \Auth::user()->id;
            $documento->id_vehiculo = $vehiculo->id;

            if (!$documento->save()){
                throw new \Exception('No se pudo guardar el documento');
            }
            
            $productos = \Input::get("id_producto");
            $cantidades = \Input::get("cantidad");
            $descuentos = \Input::get("descuento");
            $valoresDcto = \Input::get("valor_descuento");
            $valoresUnit = \Input::get("valor_unitario");
            $valoresTotal = \Input::get("valor_total");

            $valorDctoDoc = 0;
            $valorBrutoDoc = 0;
            $valorTotalDoc = 0;
            $cantidadDoc = 0;

            foreach ($productos as $k => $p){
                $cantidad = $cantidades[$k];
                if ($cantidad > 0){
                    $producto = \App\Models\Producto::find($p);

                    if (!sizeof($producto)){
                        throw new \Exception('El producto '.$p.' no existe');
                    }

                    if ($producto->tipo != "P" && $producto->tipo != "S"){
                        throw new \Exception('El tipo del producto '.$p.' - '.$producto->nombre.' no es válido');
                    }

                    if ((int)$valoresUnit[$k] <= 0){
                        throw new \Exception('El producto '.$p.' - '.$producto->nombre.' no tiene precio');
                    }

                    $movimiento = new \App\Models\Cotizacion();
                    $movimiento->id_documento = $documento->id;
                    $movimiento->id_producto = $p;
                    $movimiento->cantidad = $cantidad;
                    $movimiento->valor_unitario = $valoresUnit[$k];
                    $movimiento->descuento = $descuentos[$k];
                    $movimiento->valor_descuento = $valoresDcto[$k];
                    $movimiento->valor_total = $valoresTotal[$k];
                    $movimiento->valor_bruto = $movimiento->valor_total + $movimiento->valor_descuento;

                    $valorDctoDoc += $movimiento->valor_descuento;
                    $valorBrutoDoc += $movimiento->valor_bruto;
                    $valorTotalDoc += $movimiento->valor_total;
                    $cantidadDoc += $movimiento->cantidad;

                    if (!$movimiento->save()){
                        throw new Exception('No se pudo guardar el registro del item '.$p.' - '.$producto->nombre.' en el documento');
                    }
                }
            }

            $documento->cantidad = $cantidadDoc;
            $documento->valor_descuento = $valorDctoDoc;
            $documento->valor_iva = 0;
            $documento->valor_bruto = $valorBrutoDoc;
            $documento->valor_total = $valorTotalDoc;

            if (!$documento->save()){
                throw new Exception('No se pudo actualizar los totales del documento');
            }
            
            \DB::commit();
            \Session::forget('cotizacion.documento');
        }
        catch(\Exception $e){
            \DB::rollback();
            return $this->retornarError($e);
        }
        
        return \Redirect::action('CotizacionController@mostrarIndex')->with("mensaje", "Documento guardado exitosamente");
    }
    
    public function verDetalle($id){
        $documento = \App\Models\Documento::with(array("cotizacion.producto", "usuarioCreacion", "cliente", "tipoDocumento", "vehiculo"))->where("id", $id)->first();
        
        if (!sizeof($documento)){
            return \Redirect::action('CotizacionController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        else{
            return \View::make("cotizacion.detalle", array("documento" => $documento));
        }
    }
    
    public function imprimirPdf($id){
        $documento = \App\Models\Documento::with(array("cotizacion.producto", "usuarioCreacion", "cliente", "tipoDocumento", "vehiculo"))->where("id", $id)->first();
        
        if (!sizeof($documento)){
            return \Redirect::action('CotizacionController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        else{
            $html = \View::make("cotizacion.impresionPdf", array("documento" => $documento))->render();
            $pdf = new \App\Lib\myPdf();
            $pdf->render($html, "Documento_".$documento->tipoDocumento->sigla."_".$documento->id);
        }
    }
    
    public function enviarEmail($id){
        $documento = \App\Models\Documento::with(array("cotizacion.producto", "usuarioCreacion", "cliente", "tipoDocumento", "vehiculo"))->where("id", $id)->first();
        
        if (!sizeof($documento)){
            return \Redirect::action('CotizacionController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        else{
            
            if (empty($documento->cliente->email)){
                return \Redirect::action('CotizacionController@mostrarIndex')->with("mensajeError", "No se ha asignado una dirección de correo al cliente");
            }
            
            $nombre = "Documento_".$documento->tipoDocumento->sigla."_".$documento->id.".pdf";
            $html = \View::make("cotizacion.impresionPdf", array("documento" => $documento))->render();
            $pdf = new \App\Lib\myPdf();
            $pdf->renderToFile($html, $nombre);
            
            $datosEmail = array("documento" => $documento);
            
            \Mail::send('emails.cotizacion.documento', $datosEmail, function($message) use ($documento, $nombre) {
                $message->from('desarrollo@encubo.ws', 'Leonardo Rios');
                $message->subject('Envio de cotizacion');
                $message->to($documento->cliente->email);
                $message->bcc('desarrollo@encubo.ws');
                $message->attach(\Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."/".$nombre);
            });
            
            return \Redirect::action('CotizacionController@mostrarIndex')->with("mensaje", "Cotización enviada");
        }
    }
    
    public function generarFactura($id){
        $documento = \App\Models\Documento::with(array("cotizacion", "cliente", "tipoDocumento", "docGenerado"))->where("id", $id)->first();
        
        if (!sizeof($documento)) {
            return \Redirect::action('CotizacionController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        
        $jsonDocumento = array(
            "id_vehiculo" => $documento->id_vehiculo,
            "cedulaCliente" => $documento->cliente->id_nit,
            "detalle" => array(),
            "valorDescuento" => 0
        );
        
        $cantidad = 0;
        $valorTotal = 0;
        
        foreach ($documento->cotizacion as $p){
            $jsonDocumento["detalle"][] = array(
                "idProducto" => $p->id_producto,
                "cantidad" => $p->cantidad,
                "valorUnitario" => $p->valor_unitario,
                "descuento" => 0,
                "valorDescuento" => 0,
                "total" => $p->valor_total
            );
            
            $cantidad += $p->cantidad;
            $valorTotal += $p->valor_total;
        }
        
        $jsonDocumento["cantidad"] = $cantidad;
        $jsonDocumento["total"] = $valorTotal;
        
        //return json_encode($jsonDocumento);
        \Session::put('facturacion.documento', json_encode($jsonDocumento));
        return \Redirect::action('FacturacionController@crearFactura');
    }
}