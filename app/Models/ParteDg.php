<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ParteDg extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dg_doc_parte';
    
    public function documento(){
        return $this->belongsTo("App\Models\DocumentoDg", "id_doc_dg");
    }
    
     public function parteVehiculo(){
        return $this->belongsTo("App\Models\ParteVehiculo", "id_parte");
    }

}
