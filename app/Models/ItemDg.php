<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ItemDg extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dg_item';
    protected $fillable = array('id', 'nombre', 'id_categoria', 'requiere_cant', 'unm');
    
    public function categoria(){
        return $this->belongsTo("App\Models\CategoriaDg", "id_categoria");
    }
    
    public function tipos(){
        return $this->belongsToMany("App\Models\TipoDocumento", "dg_itemdoc", "id_item", "id_tipo");
    }
    
    public function diagnosticos(){
        return $this->hasMany("App\Models\Diagnostico", "id_item_dg");
    }
    
    public function tipo($tipoDoc){
        if (sizeof($this->tipos)){
            return array_filter($this->tipos->toArray(), function($var) use ($tipoDoc) {
                return $var["id"] === $tipoDoc;
            });
        }
        else{
            return array();
        }
        
    }
}
