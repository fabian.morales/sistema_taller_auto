<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model {    
    protected $table = 'doc_tipo';
    
    public function documentos(){
        return $this->hasMany('App\Models\Documento', 'id_tipo');
    }
}

