<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class DocumentoDg extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dg_documento';
    
    public function diagnosticos(){
        return $this->hasMany('App\Models\Diagnostico', 'id_doc_dg');
    }
    
    public function productos(){
        return $this->hasMany('App\Models\ProductoDg', 'id_doc_dg');
    }

    public function imagenes(){
        return $this->hasMany('App\Models\ImagenDg', 'id_doc_dg');
    }
    
    public function partes(){
        return $this->hasMany('App\Models\ParteDg', 'id_doc_dg');
    }
    
    public function prestamos(){
        return $this->hasMany('App\Models\Prestamo', 'id_orden');
    }
}
