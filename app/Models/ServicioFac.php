<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ServicioFac extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fac_servicio';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at');
    protected $fillable = array('id', 'id_cargo', 'id_producto', 'id_empleado', 'id_docfac');
    
    public function cargo(){
        return $this->belongsTo('App\Models\Cargo', 'id_cargo', 'id');
    }
    
    public function servicio(){
        return $this->belongsTo('App\Models\Producto', 'id_producto', 'id');
    }
    
    public function empleado(){
        return $this->belongsTo('App\User', 'id_empleado', 'id');
    }
    
    public function documentoFac(){
        return $this->belongsTo('App\Models\Factura', 'id_docfac', 'id');
    }
}