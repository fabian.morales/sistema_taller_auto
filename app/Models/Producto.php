<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cat_producto';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at');
    protected $fillable = array('id', 'nombre', 'precio', 'tipo', 'precio_abierto');
    
    public function encargados(){
        return $this->hasMany('App\Models\ServicioCargo', 'id_producto');
    }
    
    public function items(){
        return $this->hasMany("App\Models\ProductoDg", "id_producto");
    }
        
    /*public static function boot()
    {
        parent::boot();
        
        static::created(function($producto)
        {
            $log = new DataLog();
            $log->tabla = "Producto";
            $log->objeto = $producto->toJson();
            $log->id_usuario = Auth::user()->id;
            $log->save();
        });

        static::updated(function($producto)
        {
            $log = new DataLog();
            $log->tabla = "Producto";
            $log->objeto = $producto->toJson();
            $log->id_usuario = Auth::user()->id;
            $log->save();
        });
    } */ 

}