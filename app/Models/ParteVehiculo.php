<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ParteVehiculo extends Model {    
    protected $table = 'par_vehiculo_parte';
    protected $fillable = array('id', 'nombre', 'llave');
    
    public function tipos(){
        return $this->belongsToMany("App\Models\TipoVehiculo", "par_vehiculo_tipo_parte", "id_parte", "id_tipo")->withPivot('path');
    }
}
