<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model {    
    protected $table = 'fac_documento';
    
    public function producto(){
        return $this->hasOne('App\Models\Producto', 'id', 'id_producto');
    }
    
    public function servicios(){
        return $this->hasMany('App\Models\ServicioFac', 'id_docfac');
    }
}
