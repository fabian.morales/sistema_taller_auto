<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class OpcionDg extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dg_opcion';
    protected $fillable = array('id', 'nombre');
}
