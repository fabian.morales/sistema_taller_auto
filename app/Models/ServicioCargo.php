<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ServicioCargo extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cat_servicio_cargo';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at');
    protected $fillable = array('id', 'id_cargo', 'id_producto', 'cantidad', 'porcentaje');
    
    public function cargo(){
        return $this->belongsTo('App\Models\Cargo', 'id_cargo', 'id');
    }
    
    public function servicio(){
        return $this->belongsTo('App\Models\Producto', 'id_producto', 'id');
    }
}