<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProductoDg extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dg_doc_producto';
    
    public function documento(){
        return $this->belongsTo("App\Models\DocumentoDg", "id_doc_dg");
    }
    
    public function producto(){
        return $this->belongsTo("App\Models\Producto", "id_producto");
    }

}
