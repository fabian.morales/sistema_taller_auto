<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Documento extends Model {
    protected $table = 'doc_documento';

    public function tipoDocumento(){
        return $this->belongsTo('App\Models\TipoDocumento', 'id_tipo');
    }

    public function movimientos(){
        return $this->hasMany('App\Models\Movimiento', 'id_documento');
    }

    public function factura(){
        return $this->hasMany('App\Models\Factura', 'id_documento');
    }

    public function docDiagnostico(){
        return $this->hasMany('App\Models\DocumentoDg', 'id_documento');
    }

    public function abonos(){
        return $this->hasMany('App\Models\AbonoFac', 'id_documento');
    }

    public function cliente(){
        return $this->hasOne('App\Models\Cliente', 'id', 'id_cliente');
    }

    public function usuarioCreacion(){
        return $this->hasOne('App\User', 'id', 'id_usuario_crea');
    }

    public function vehiculo(){
        return $this->hasOne('App\Models\Vehiculo', 'id', 'id_vehiculo');
    }

    public function docGenerado(){
        return $this->belongsTo('App\Models\Documento', 'id', 'id_doc_rel');
    }

    public function relacionado(){
        return $this->hasOne('App\Models\Documento', 'id', 'id_doc_rel');
    }

    public function cotizacion(){
        return $this->hasMany('App\Models\Cotizacion', 'id_documento');
    }
    
    function periodo(){
        return $this->belongsToMany('App\Models\Periodo', 'nom_periodo_documento', 'id_documento', 'id_periodo');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($documento)
        {
            $consec = \App\Models\Consecutivo::where("id_tipo", $documento->id_tipo)->first();
            if (!sizeof($consec)){
                $consec = new \App\Models\Consecutivo();
                $consec->consecutivo = 1;
                $consec->id_tipo = $documento->id_tipo;
            }
            else{
                $consec->consecutivo += 1;
            }

            $documento->num = $consec->consecutivo;
            $documento->save();
            $consec->save();
        });
    }
}
