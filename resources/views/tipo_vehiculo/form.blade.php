@extends('master')

@section('js_header')

@stop

@section('content')
<h2>Datos del tipo de veh&iacute;culo</h2>
<form id="form_tipo" name="form_tipo" action="{{ url('vehiculos/tipos/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $tipo->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-4 small-12 columns end">
            <input type="text" name="nombre" id="nombre" value="{{ $tipo->nombre }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="imagen">Imagen</label>
        </div>
        <div class="medium-4 small-12 columns end">
            <input type="file" name="imagen" id="imagen" />
        </div>
    </div>
    @if(!empty($img))
    <div class="row">
        <div class="medium-6 small-12 columns end">
            <img src="{{ asset($img) }}" />
        </div>
    </div>
    @endif
    <fieldset>
        <legend>Partes de veh&iacute;culo asociadas</legend>
        @foreach($partes as $p)
        <div class="row">
            <div class="small-3 columns">
                <label for="assoc_partes_{{ $p->id }}">
                    <input type="checkbox" id="assoc_partes_{{ $p->id }}" name="assoc_partes[{{ $p->id }}]" @if(sizeof($p->tipos)) checked="checked" @endif />
                    {{ $p->nombre }}
                </label>
            </div>
            <div class="small-9 columns">
                <input type="text" name="partes[{{ $p->id }}]" id="partes_{{ $p->id }}" value="@if(sizeof($p->tipos)){{ $p->tipos[0]->pivot->path }}@endif" />
            </div>
        </div>
        @endforeach
    </fieldset>
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/vehiculos/partes/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop