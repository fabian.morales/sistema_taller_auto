<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('vehiculos/tipos/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de tipos de veh&iacute;culos</div>
</div>
<div class="row item lista">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-3 columns">Nombre</div>
    <div class="small-2 columns">Editar</div>
</div>
@foreach($tipos as $t)
<div class="row item lista">
    <div class="small-2 columns">{{ $t->id }}</div>
    <div class="small-3 columns">{{ $t->nombre }}</div>
    <div class="small-2 columns"><a href="{{ url('/vehiculos/tipos/editar/'.$t->id) }}"><i class="fi-pencil"></i></a></div>
</div>        
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $tipos->render() !!}
    </div>
</div>