@extends('master')

@section('js_header')
<script>
(function($, window){
    $(document).ready(function() {       
        $("#btnBuscarUsr").click(function(e) {
            e.preventDefault();
            $("#loader").addClass("loading");
            $.ajax({
                url: '{{ asset("/usuarios/lista") }}',
                method: 'post',
                data: $("#form_buscar_usr").serialize()
            })
            .done(function(res) {
                $("#div_usuarios").html(res);
            })
            .always(function() {
                $("#loader").removeClass("loading");
            });
        });
        
        $("#btnLimpiar").click(function(e) {
            e.preventDefault();
            $("#form_buscar_usr input").each(function(i, o) {
                $(o).val('');
            });
            
            $("#btnBuscarUsr").click();
        });
    });
})(jQuery, window);
</script>
@stop

@section('content')
<fieldset>
    <legend>Buscador</legend>
    <form id="form_buscar_usr">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="row">
            <div class="small-12 medium-1 columns"><label for="nombre">Nombre</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="nombre" id="nombre" /></div>
            <div class="small-12 medium-1 columns"><label for="cedula">C&eacute;dula</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="cedula" id="cedula" /></div>
            <div class="small-12 medium-3 columns end">
                <button class="tiny button rojo left" id="btnBuscarUsr">Buscar</button>
                <button class="tiny button left" id="btnLimpiar">Limpiar</button>
            </div>
        </div>
    </form>
</fieldset>
<div id="div_usuarios">
@include('usuario.lista', array("usuarios" => $usuarios))
</div>
@stop