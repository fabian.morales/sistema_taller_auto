<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('usuarios/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de usuarios</div>
</div>
<div class="row item lista">
    <div class="small-1 columns">N&uacute;m</div>
    <div class="small-3 columns">Nombre</div>
    <div class="small-2 columns">C&eacute;dula</div>
    <div class="small-2 columns">Cargo</div>
    <div class="small-3 columns">Correo</div>
    <div class="small-1 columns">Editar</div>
</div>
@foreach($usuarios as $u)
<div class="row item lista">
    <div class="small-1 columns">{{ $u->id }}</div>
    <div class="small-3 columns">{{ $u->nombre }}</div>
    <div class="small-2 columns">{{ $u->cedula }}</div>
    <div class="small-2 columns">@if (sizeof($u->cargos)) {{ $u->cargos[0]->nombre }} @endif</div>
    <div class="small-3 columns">{{ $u->email }}</div>
    <div class="small-1 columns"><a href="{{ url('/usuarios/editar/'.$u->id) }}"><i class="fi-pencil"></i></a></div>
</div>                
<!--a href="{{ url('/usuarios/permisos/'.$u->id) }}"><i class="fi-checkbox"></i></a-->
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $usuarios->render() !!}
    </div>
</div>