@extends('master')

@section('js_header')
<script src="{{ asset('js/accounting.min.js') }}"></script>
<script>
(function ($, _, $ac, window) {
    var $jsonDocumento = {};
    
    var listaProductos = [@foreach($productos as $p){ value: "{{ $p->id }}", label: "{!! $p->nombre !!}", id: "{{ $p->id }}" },@endforeach];

    function enviarSesion() {
        $.ajax({
            url: '{{ url("/facturas/sesion/") }}',
            data: {json: $jsonDocumento, _token: '{!! csrf_token() !!}'},
            method: 'post'
        });
    }

    function obtenerSesion() {
        $("#loader").addClass("loading");
        $.ajax({
            url: '{{ url("/facturas/recuperar/") }}',
            method: 'post',
            data: {_token: '{!! csrf_token() !!}'},
            dataType: 'json',
            success: function (json) {
                if (!_.isEmpty(json)) {
                    console.log(json);

                    if (!_.isEmpty(json.cedulaCliente)) {
                        $("#cedula").val(json.cedulaCliente);
                        $("#btnBuscarCliente").click();
                    }

                    $jsonDocumento.id_vehiculo = json.id_vehiculo;
                    $jsonDocumento.id_doc_rel = _.isEmpty(json.id_doc_rel) ? '' : json.id_doc_rel;
                    $("#id_doc_rel").val($jsonDocumento.id_doc_rel);

                    if (!_.isEmpty(json.detalle)) {
                        var $first = true;
                        $.each(json.detalle, function (i, o) {
                            var $o = undefined;
                            if ($first === true) {
                                $o = $("#registro");
                                $first = false;
                            } else {
                                $o = adicionarRegistroItem();
                            }

                            asignarValoresItem($o, o);
                        });

                        calcularSubtotales();
                    }

                    if (!_.isEmpty(json.abonos)) {
                        var $first = true;
                        $.each(json.abonos, function (i, o) {
                            var $o = undefined;
                            if ($first === true) {
                                $o = $("#medio_pago");
                                $first = false;
                            } else {
                                $o = adicionarRegistroMP();
                            }

                            var $txtAbono = $o.find("input[data-id='monto_pago']");
                            var $cbxMedio = $o.find("select[data-id='medio_pago']");
                            $txtAbono.val(o.valorAbono);
                            $cbxMedio.val(o.idMedio);
                        });

                        procesarMedioPago();
                    }
                }
            }
        }).
        always(function () {
            $("#loader").removeClass("loading");
        });
    }

    function asignarValoresItem($o, $data) {
        if (!_.isEmpty($data)) {
            var $txtCantidad = $o.find("input[data-id='cantidad']");
            var $txtUnitario = $o.find("input[data-id='unitario']");
            var $txtTotal = $o.find("input[data-id='total']");
            var $valorTotal = $o.find("input[data-id='valorTotal']");
            var $txtDescuento = $o.find("input[data-id='descuento']");
            var $valorDescuento = $o.find("input[data-id='valorDescuento']");
            var $txtUnitario = $o.find("input[data-id='unitario']");
            var $valorUnitario = $o.find("input[data-id='valorUnitario']");
            var $cbxProducto = $o.find("select[data-id='producto']");
            var $txtFiltro = $o.find("input[data-id='filtro']");
            $cbxProducto.val($data.idProducto);
            $txtFiltro.val($cbxProducto.find('option:selected').html());

            var $cantidad = _.isNaN($data.cantidad) ? 1 : parseInt($data.cantidad);
            var $valUnitario = _.isNaN($data.valorUnitario) ? parseFloat($cbxProducto.find('option:selected').attr('data-precio')) : parseFloat($data.valorUnitario);
            var $descuento = parseFloat($data.descuento);

            $txtCantidad.val($cantidad);
            $txtUnitario.val($ac.formatMoney($valUnitario));
            $valorUnitario.val($valUnitario);
            $txtDescuento.val($descuento);

            $valorDescuento.val(($cantidad * $valUnitario) * ($descuento / 100));
            var $valTotal = ($cantidad * $valUnitario) * (1 - $descuento / 100);
            $txtTotal.val($ac.formatMoney($valTotal));
            $valorTotal.val($valTotal);

            var $soloLectura = $cbxProducto.find('option:selected').attr('data-abierto') !== 'Y';
            if ($soloLectura === true) {
                $txtUnitario.attr('readonly', 'readonly');
            } else {
                $txtUnitario.removeAttr('readonly');
            }
        }
    }

    function adicionarRegistroItem() {
        var $html = $("#registro").html();
        var $o = $($.parseHTML($html));
        var $cnt = $("#registros > div.row").size();
        $o.attr("id", "registro" + $cnt + Math.floor((Math.random() * 1000) + 1));
        hookRegistro($o, true);
        $("#registros").append($o);
        return $o;
    }

    function hookRegistro($o, $nuevo) {
        var $txtCantidad = $o.find("input[data-id='cantidad']");
        var $txtUnitario = $o.find("input[data-id='unitario']");
        var $valorUnitario = $o.find("input[data-id='valorUnitario']");
        var $txtTotal = $o.find("input[data-id='total']");
        var $valorTotal = $o.find("input[data-id='valorTotal']");
        var $txtDescuento = $o.find("input[data-id='descuento']");
        var $valorDescuento = $o.find("input[data-id='valorDescuento']");
        var $cbxProducto = $o.find("select[data-id='producto']");
        var $remover = $o.find("a[data-id='quitar']");
        var $servicios = $o.find("a[data-id='servicios']");
        var $filtro = $o.find("input[data-id='filtro']");

        if ($nuevo === true) {
            $txtCantidad.val(1);
        }

        var mostrarFormServicios = function () {
            var $tipo = $cbxProducto.find('option:selected').attr('data-tipo');
            if ($tipo === 'S') {
                $("#loader").addClass("loading");
                $.featherlight('{{ url("/facturas/servicios/") }}/' + $cbxProducto.val(), {
                    afterOpen: function () {
                        $("#loader").removeClass("loading");
                        $("#btnGuardarServicio").click(function () {
                            $.ajax({
                                url: $("#form_servicios").attr("action"),
                                data: $("#form_servicios").serialize(),
                                method: 'post',
                                success: function (res) {
                                    //console.log(res);
                                    $.featherlight.current().close();
                                }
                            });
                        });

                        $("#btnCancelar").click(function (e) {
                            e.preventDefault();
                            $.featherlight.current().close();
                        });
                    }
                });
            }
        };
        
        $filtro.autocomplete({
            source: listaProductos,
            autoFocus: true,
            position: { my : "left top", at: "left bottom" },
            select: function( event, ui ) {
                $(this).val(ui.item.label);
                $cbxProducto.val(ui.item.id);
                $cbxProducto.change();
                return false;
            }
        });

        $cbxProducto.change(function () {
            $valorUnitario.val($cbxProducto.find('option:selected').attr('data-precio'));
            var $precio = calcularPrecio();

            $txtUnitario.val($ac.formatMoney($precio.unitario));
            $txtTotal.val($ac.formatMoney($precio.total));
            $valorTotal.val($precio.total);
            $valorDescuento.val($precio.descuento);
            calcularSubtotales();

            var $soloLectura = $cbxProducto.find('option:selected').attr('data-abierto') !== 'Y';
            if ($soloLectura === true) {
                $txtUnitario.attr('readonly', 'readonly');
            } else {
                $txtUnitario.removeAttr('readonly');
            }

            mostrarFormServicios();
        });

        $txtUnitario.change(function (e) {
            var $precio = parseFloat($(this).val().replace("$", "").replace(".", "")); //$cbxProducto.find('option:selected').attr('data-precio');

            /*if (parseFloat($precio) > parseFloat($(this).val())){
             alert('El precio está por debajo del mínimo');
             $(this).val($ac.formatMoney($precio));
             }
             else{
             $precio = $(this).val();
             }*/

            $valorUnitario.val($precio);
            $precio = calcularPrecio($precio);
            $(this).val($ac.formatMoney($precio.unitario));
            $txtTotal.val($ac.formatMoney($precio.total));
            $valorTotal.val($precio.total);
            $valorDescuento.val($precio.descuento);
            calcularSubtotales();

        });

        $txtCantidad.change(function () {
            var $precio = calcularPrecio();
            $txtTotal.val($ac.formatMoney($precio.total));
            $valorTotal.val($precio.total);
            $valorDescuento.val($precio.descuento);
            calcularSubtotales();
        });

        $txtDescuento.change(function () {
            var $precio = calcularPrecio();
            $txtTotal.val($ac.formatMoney($precio.total));
            $valorTotal.val($precio.total);
            $valorDescuento.val($precio.descuento);
            calcularSubtotales();
        });

        $remover.click(function (e) {
            e.preventDefault();
            if ($o.attr("id") !== "registro") {
                $o.remove();
                calcularSubtotales();
            }
        });

        $servicios.click(function (e) {
            e.preventDefault();
            mostrarFormServicios();
        });

        var calcularPrecio = function ($_precio) {
            var $precio = parseFloat($valorUnitario.val()); //$_precio === undefined ? parseFloat($cbxProducto.find('option:selected').attr('data-precio')) : parseFloat($_precio);
            var $ret = {total: 0, unitario: 0, descuento: 0};

            if ($precio > 0) {
                $ret.unitario = $precio;
                $ret.descuento = (parseInt($txtCantidad.val()) * $precio) * (parseFloat($txtDescuento.val()) / 100);
                $ret.total = (parseInt($txtCantidad.val()) * $precio) * (1 - parseFloat($txtDescuento.val()) / 100);
            }

            return $ret;
        };
    }

    function adicionarRegistroMP() {
        var $html = $("#medio_pago").html();
        var $o = $($.parseHTML($html));
        var $cnt = $("#medios_pago > div.row").size();
        $o.attr("id", "medio_pago" + $cnt + Math.floor((Math.random() * 1000) + 1));
        hookRegistroMP($o, true);
        $("#medios_pago").append($o);

        return $o;
    }

    function hookRegistroMP($o, $nuevo) {
        var $txtAbono = $o.find("input[data-id='monto_pago']");
        var $cbxMedio = $o.find("select[data-id='medio_pago']");
        var $remover = $o.find("a[data-id='quitar']");

        $cbxMedio.change(function () {
            procesarMedioPago();
        });

        $txtAbono.change(function (e) {
            procesarMedioPago();
        });

        $remover.click(function (e) {
            e.preventDefault();
            if ($o.attr("id") !== "medio_pago") {
                $o.remove();
                procesarMedioPago();
                //calcularSubtotales();
            }
        });
    }

    function procesarMedioPago() {
        $jsonDocumento.abonos = [];
        var $totalAbonos = 0;
        $("div[rel='medio_pago']").each(function (i, o) {
            var $cbxMedio = $(o).find("select[data-id='medio_pago']");
            var $txtAbono = $(o).find("input[data-id='monto_pago']");

            var $item = {
                idMedio: $cbxMedio.val(),
                valorAbono: parseFloat($txtAbono.val())
            };

            $totalAbonos += $item.valorAbono;
            $jsonDocumento.abonos.push($item);
        });

        $("#total_abonos").val($ac.formatMoney($totalAbonos));
        $("#total_pendiente").val($jsonDocumento.total - $totalAbonos);

        enviarSesion();

        return $totalAbonos;
    }

    function calcularSubtotales() {
        var $totalItems = 0;
        var $totalIva = 0;
        var $totalDescuento = 0;
        var $totalDocumento = 0;

        $jsonDocumento.detalle = [];

        $("div[rel='registro']").each(function (i, o) {
            var $txtCantidad = $(o).find("input[data-id='cantidad']");
            var $txtUnitario = $(o).find("input[data-id='unitario']");
            var $valorUnitario = $(o).find("input[data-id='valorUnitario']");
            var $txtDescuento = $(o).find("input[data-id='descuento']");
            var $valorDescuento = $(o).find("input[data-id='valorDescuento']");
            var $txtTotal = $(o).find("input[data-id='total']");
            var $valorTotal = $(o).find("input[data-id='valorTotal']");
            //var $txtDescuento = $(o).find("input[data-id='descuento']");
            var $cbxProducto = $(o).find("select[data-id='producto']");
            var $precio = parseFloat($cbxProducto.find('option:selected').attr('data-precio'));

            if ($precio > 0) {
                var $item = {
                    idProducto: $cbxProducto.find('option:selected').val(),
                    cantidad: parseInt($txtCantidad.val()),
                    valorUnitario: parseInt($valorUnitario.val()),
                    descuento: parseInt($txtDescuento.val()),
                    valorDescuento: parseInt($valorDescuento.val()),
                    total: parseInt($valorTotal.val())
                };

                $totalItems += $item.cantidad;
                $totalDescuento += $item.valorDescuento;
                $totalDocumento += $item.total;

                $jsonDocumento.detalle.push($item);
            }
        });

        $jsonDocumento.cantidad = $totalItems;
        $jsonDocumento.valorDescuento = $totalDescuento;
        $jsonDocumento.total = $totalDocumento;
        $jsonDocumento.cedulaCliente = $("#cedula").val();

        enviarSesion();

        $("#total_items").val($totalItems);
        $("#total_descuentos").val($ac.formatMoney($totalDescuento));
        $("#total_documento").val($ac.formatMoney($totalDocumento));

        var $ret = {
            cantidad: $totalItems,
            total: $totalDocumento,
            totalDescuento: $totalDescuento
        };

        return $ret;
    }
    
    $(window).bind("beforeunload", function() {
        return "¿Está seguro de querer salir?";
    });

    $(document).ready(function () {
        $ac.settings = {
            currency: {
                symbol: "$", // default currency symbol is '$'
                format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
                decimal: ",", // decimal point separator
                thousand: ".", // thousands separator
                precision: 0   // decimal places
            },
            number: {
                precision: 0, // default precision on numbers is 0
                thousand: ".",
                decimal: ","
            }
        };

        $("#btnAdicionarReg").click(function (e) {
            e.preventDefault();
            adicionarRegistroItem();
        });

        $("#btnAdicionarMP").click(function (e) {
            e.preventDefault();
            adicionarRegistroMP();
        });

        $("#btnBuscarCliente").click(function (e) {
            e.preventDefault();
            $("#loader").addClass("loading");
            $.ajax({
                url: '{{ url("/clientes/buscar") }}',
                method: 'post',
                data: {'cedula': $("#cedula").val(), _token: '{!! csrf_token() !!}'},
                dataType: 'json',
                success: function (json) {
                    if (!_.isEmpty(json)) {

                        $("#nombre").val(json.nombres);
                        $("#apellidos").val(json.apellidos);
                        $("#telefono").val(json.telefono);
                        $("#direccion").val(json.direccion);

                        $jsonDocumento.cedulaCliente = $("#cedula").val();
                        $("#id_vehiculo").html('');
                        $("#id_vehiculo").append('<option id=""></option>');
                        $.each(json.vehiculos, function (i, o) {
                            $("#id_vehiculo").append('<option value="' + o.id + '">' + o.nombre + ' - ' + o.placa + '</option>');
                        });

                        if (!_.isEmpty($jsonDocumento.id_vehiculo)) {
                            $("#id_vehiculo").val($jsonDocumento.id_vehiculo);
                        }

                        enviarSesion();
                    } else {
                        alert('Cliente no encontrado');
                    }

                    $("#loader").removeClass("loading");
                }
            });
        });

        $("#id_vehiculo").change(function (e) {
            e.preventDefault();
            $jsonDocumento.id_vehiculo = $(this).val();
            enviarSesion();
            
            if (!_.isEmpty($jsonDocumento.id_vehiculo)){
                $("#loader").addClass("loading");
                $.ajax({
                    url: '{{ url("/facturas/buscarVehiculo") }}/' + $(this).val(),
                    method: 'post',
                    data: {_token: '{!! csrf_token() !!}'},
                    dataType: 'json',
                    success: function (json) {
                        if (!_.isEmpty(json)) {
                            $("#marca").val(json.marca);
                            $("#motor").val(json.motor);
                            $("#serie").val(json.serie);
                            $("#color").val(json.color);
                            $("#placa").val(json.placa);
                            $("#modelo").val(json.modelo);
                            $("#km").val(json.km);
                            $("#cilindraje").val(json.cilindraje);
                            $("#caja").val(json.caja);
                            $("#tipo_vehiculo").val(json.id_tipo);
                        } else {
                            alert('Vehículo no encontrado');
                        }

                        $("#loader").removeClass("loading");
                    }
                });
            }
        });

        $("#btnGuardarDocumento").click(function (e) {
            e.preventDefault();
            if ($("#cedula").val() === '') {
                alert('Debe ingresar la cédula del cliente');
                return;
            }

            if ($("#nombre").val() === '') {
                alert('Debe ingresar el nombre del cliente');
                return;
            }

            if ($("#apellidos").val() === '') {
                alert('Debe ingresar los apellidos del cliente');
                return;
            }
            
            if ($("#tipo_vehiculo").val() === '') {
                alert('Debe seleccionar el tipo de vehículo');
                return;
            }
            
            if ($("#marca").val() === '') {
                alert('Debe ingresar la marca del vehículo');
                return;
            }

            if ($("#motor").val() === '') {
                alert('Debe ingresar el tipo de motor del vehículo');
                return;
            }

            if ($("#color").val() === '') {
                alert('Debe ingresar el color del vehículo');
                return;
            }

            if ($("#serie").val() === '') {
                alert('Debe ingresar la serie del vehículo');
                return;
            }

            if ($("#placa").val() === '') {
                alert('Debe ingresar la placa del vehículo');
                return;
            }

            if ($("#km").val() === '') {
                alert('Debe ingresar el kilometraje del vehículo');
                return;
            }

            if ($("#cilindraje").val() === '') {
                alert('Debe ingresar el cilindraje del vehículo');
                return;
            }

            if ($("#caja").val() === '') {
                alert('Debe ingresar el tipo de caja del vehículo');
                return;
            }

            var $totales = calcularSubtotales();
            var $totalAbonos = procesarMedioPago();

            if ($totales.cantidad <= 0) {
                alert('Revise los items ingresados en el documento');
                return;
            }

            if ($totales.total <= 0) {
                alert('Revise los precios de los items ingresados en el documento');
                return;
            }

            if ($totales.total !== $totalAbonos) {
                alert('El total de abonos no coincide con el total del documento');
                return;
            }

            if ($("#observaciones").val() === '') {
                if (!confirm('No ha ingresado las observaciones, ¿desea continuar?')) {
                    return;
                }
            }

            if (!confirm('¿Está seguro de terminar este documento?')) {
                return;
            }

            $("#loader").addClass("loading");

            $.ajax({
                url: '{{ url("/facturas/guardar") }}',
                data: $("#form_documento").serialize(),
                method: 'post'
            })
            .done(function (res) {
                alert('El documento ha sido guardado exitosamente');
                window.location.reload();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseJSON.error.message);
            })
            .always(function () {
                $("#loader").removeClass("loading");
            });
        });

        hookRegistro($("#registro"), false);
        hookRegistroMP($("#medio_pago"), false);
        obtenerSesion();
    });
})(jQuery, window._, accounting, window);
</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h2>Cuenta de cobro</h2>
        <br />
    </div>
</div>
<form id="form_documento" name="form_documento" action="{{ url('facturas/guardar') }}" method="post">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id_doc_rel" name="id_doc_rel" value="">
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="nombre">Tipo</label>
        </div>
        <div class="medium-4 small-12 columns end">
            <select id="id_tipo" name="id_tipo">
                @foreach($tipos as $t)
                <option value="{{ $t->id }}">{{ $t->sigla }} - {{ $t->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <fieldset>
        <legend>Datos del cliente</legend>
        <div class="row">
            <div class="small-12 medium-2 columns"><label for="cedula">C&eacute;dula</label></div>
            <div class="small-12 medium-4 columns">
                <div class="row collapse">
                    <div class="small-9 columns"><input type="text" id="cedula" name="cedula" /></div>
                    <div class="small-3 columns"><button class="button default rojo tiny" id="btnBuscarCliente">Buscar</button></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="nombre">Nombre</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="nombre" name="nombre" /></div>
            <div class="small-12 medium-2 columns"><label for="apellidos">Apellidos</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="apellidos" name="apellidos" /></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="telefono">Tel&eacute;fono</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="telefono" name="telefono" /></div>
            <div class="small-12 medium-2 columns"><label for="direccion">Direcci&oacute;n</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="direccion" name="direccion" /></div>
            <div class="clearfix"></div>
        </div>
    </fieldset>
    
    <fieldset>
        <legend>Datos de veh&iacute;culo</legend>
        <div class="row">
            <div class="small-12 medium-2 columns"><label for="vehiculo">Veh&iacute;culo</label></div>
            <div class="small-12 medium-4 columns end">
                <select id="id_vehiculo" name="id_vehiculo"></select>
            </div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="tipo_vehiculo">Tipo veh&iacute;culo</label></div>
            <div class="small-12 medium-4 columns">
                <select id="tipo_vehiculo" name="tipo_vehiculo">
                    <option value=""></option>
                    @foreach($tipos_veh as $t)
                    <option value="{{ $t->id }}">{{ $t->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="small-12 medium-2 columns"><label for="marca">Marca</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="marca" name="marca" /></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="motor">Motor</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="motor" name="motor" /></div>
            <div class="small-12 medium-2 columns"><label for="serie">Serie</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="serie" name="serie" /></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="color">Color</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="color" name="color" /></div>
            <div class="small-12 medium-2 columns"><label for="placa">Placa</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="placa" name="placa" /></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="km">Km</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="km" name="km" /></div>
            <div class="small-12 medium-2 columns"><label for="cilindraje">Cil. motor</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="cilindraje" name="cilindraje" /></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="caja">Caja</label></div>
            <div class="small-12 medium-4 columns end"><input type="text" id="caja" name="caja" /></div>
            <div class="small-12 medium-2 columns"><label for="modelo">Modelo</label></div>
            <div class="small-12 medium-4 columns end"><input type="text" id="modelo" name="modelo" /></div>
            <div class="clearfix"></div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Totales</legend>
        <div class="row">
            <div class="small-12 medium-2 columns"><label for="total_items">Total items</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="total_items" name="total_items" readonly="readonly" class="text-right" /></div>

            <div class="small-12 medium-2 columns"><label for="total_iva">Total Iva</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="total_iva" name="total_iva" readonly="readonly" class="text-right" /></div>

            <div class="small-12 medium-2 columns"><label for="total_descuentos">Total descuentos</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="total_descuentos" name="total_descuentos" readonly="readonly" class="text-right" /></div>

            <div class="small-12 medium-2 columns"><label for="total_documento">Total documento</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="total_documento" name="total_documento" readonly="readonly" class="text-right" /></div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Productos y servicios</legend>
        <div class="row">
            <div class="medium-3 small-12 columns">
                <strong>Producto</strong>
            </div>
            <div class="medium-2 small-12 columns text-right">
                <strong>Cantidad</strong>
            </div>
            <div class="medium-2 small-12 columns text-right">
                <strong>Descuento</strong>
            </div>
            <div class="medium-2 small-12 columns text-right">
                <strong>Vlr. Unitario</strong>
            </div>
            <div class="medium-2 small-12 columns text-right end">
                <strong>Total</strong>
            </div>
            <hr />
        </div>
        <div id="registro">
            <div class="row" rel="registro">
                <div class="medium-3 small-12 columns">
                    <input type="text" id="filtro" name="filtro[]" data-id="filtro" />
                    <select data-id="producto" name="id_producto[]" class="hide">
                        <option>Seleccione un producto</option>
                        @foreach($productos as $p)
                        <option value="{{ $p->id }}" data-precio="{{ $p->precio }}" data-abierto="{{ $p->precio_abierto }}" data-tipo="{{ $p->tipo }}">{{ $p->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="medium-2 small-12 columns">
                    <input type="text" name="cantidad[]" data-id="cantidad" value="1" class="text-right" />
                </div>
                <div class="medium-2 small-12 columns">
                    <input type="text" name="descuento[]" data-id="descuento" value="0" class="text-right" />
                    <input type="hidden" name="valor_descuento[]" data-id="valorDescuento" value="0" />
                </div>
                <div class="medium-2 small-12 columns">
                    <input type="text" name="unitario[]" data-id="unitario" value="0" readonly="readonly" class="text-right" />
                    <input type="hidden" name="valor_unitario[]" data-id="valorUnitario" value="0" />
                </div>
                <div class="medium-2 small-11 columns">
                    <input type="text" name="total[]" data-id="total" value="0" readonly="readonly" class="text-right" />
                    <input type="hidden" name="valor_total[]" data-id="valorTotal" value="0" />
                </div>
                <div class="small-1 columns">
                    <a href="#" data-id="quitar" title="Quitar producto"><i class="fi-x-circle">&nbsp;</i></a>
                    <a href="#" data-id="servicios" title="Asignar empleados a servicio"><i class="fi-wrench">&nbsp;</i></a>
                </div>
            </div>
        </div>
        <div id="registros">

        </div>
        <div class="row">
            <div class="small-12 columns">
                <a class="small button rojo right" href="#" id="btnAdicionarReg" />A&ntilde;adir registro</a>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Medios de pago</legend>
        <div class="row">
            <div class="medium-4 small-12 columns">
                <strong>Medio de pago</strong>
            </div>
            <div class="medium-2 small-12 columns text-right end">
                <strong>Monto</strong>
            </div>
            <hr />
        </div>
        <div id="medio_pago">
            <div class="row" rel="medio_pago">
                <div class="medium-4 small-12 columns">
                    <select data-id="medio_pago" name="id_medio[]">
                        <option>Seleccione un medio de pago</option>
                        @foreach($medios as $m)
                        <option value="{{ $m->id }}">{{ $m->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="medium-2 small-12 columns">
                    <input type="text" name="monto_pago[]" data-id="monto_pago" value="0" class="text-right" />
                </div>
                <div class="small-1 columns">
                    <a href="#" data-id="quitar"><i class="fi-x-circle">&nbsp;</i></a>
                </div>
            </div>
        </div>
        <div id="medios_pago">

        </div>
        <div class="row">
            <div class="small-12 columns">
                <a class="small button rojo right" href="#" id="btnAdicionarMP" />A&ntilde;adir abono</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="small-12 columns">
                <div class="small-12 medium-2 columns"><label for="total_abonos">Total abonos</label></div>
                <div class="small-12 medium-4 columns"><input type="text" id="total_abonos" name="total_abonos" readonly="readonly" class="text-right" /></div>
                <div class="small-12 medium-2 columns"><label for="total_abonos">Total pediente</label></div>
                <div class="small-12 medium-4 columns"><input type="text" id="total_pendiente" name="total_pendiente" readonly="readonly" class="text-right" /></div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Otros</legend>
        <div class="row">
            <div class="small-12 columns">
                <div class="small-12 columns"><label for="observaciones">Observaciones</label></div>
                <div class="small-12 columns"><textarea id="observaciones" name="observaciones" rows="5"></textarea></div>
            </div>
        </div>
    </fieldset>

    <div class="row">
        <div class="small-12 columns text-right">
            <a class="button gris" href="{{ url('/facturas/') }}">Cancelar</a>
            <input type="button" value="Guardar" class="button naranja" id="btnGuardarDocumento" />
        </div>
    </div>
</form>
@stop