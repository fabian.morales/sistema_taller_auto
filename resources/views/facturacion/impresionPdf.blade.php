@extends('masterPdf')

@section('content')
    <table cellspacing="5" cellpadding="0" class="sep" style="width: 100%;">
        <tr>
            <td style="width: 15%;"><strong>Documento</strong></td>
            <td>{{  $documento->tipoDocumento->nombre . ' ' . $documento->tipoDocumento->sigla }} - {{ $documento->num }}</td>
            @if(sizeof($documento->vehiculo))        
                <td style="width: 15%;"><strong>Placa:</strong></td>
                <td>{{ $documento->vehiculo->placa }}</td>
            @endif
        </tr>
        <tr>
            <td><strong>Fecha:</strong></td>
            <td>{{ date('F d Y', strtotime($documento->fecha)) }}</td>
            @if(sizeof($documento->vehiculo))        
                <td><strong>Marca:</strong></td>
                <td>{{ $documento->vehiculo->marca }}</td>
            @endif
        </tr>
        <tr>
            <td><strong>Cliente:</strong></td>
            <td>{{ $documento->cliente->nombres }} {{ $documento->cliente->apellidos }}</td>
            @if(sizeof($documento->vehiculo))        
                <td><strong>Linea o nombre:</strong></td>
                <td>{{ $documento->vehiculo->nombre }}</td>
            @endif
        </tr>
        <tr>
            <td><strong>C&eacute;dula:</strong></td>
            <td>{{ $documento->cliente->id_nit }}</td>
            @if(sizeof($documento->vehiculo))        
                <td><strong>Tipo:</strong></td>
                <td>{{ $documento->vehiculo->tipo->nombre }}</td>
            @endif
        </tr>
        
        @if(sizeof($documento->vehiculo))
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>        
            <td><strong>Color:</strong></td>
            <td>{{ $documento->vehiculo->color }}</td>
        </tr>
        @endif
        
        @if(sizeof($documento->relacionado))
        <tr>
        <td><strong>Relacionado: </strong></td>
        <td>{{ $documento->relacionado->tipoDocumento->nombre . ' ' . $documento->relacionado->tipoDocumento->sigla . ' - ' . $documento->relacionado->num }}</td>
        </tr>
        @endif
    </table>
    
    <h2>Productos y servicios</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr class='doble'>
                <td><strong>Producto</strong></td>
                <td class="right"><strong>Cant.</strong></td>
                <td class="right"><strong>Dcto.</strong></td>
                <td class="right"><strong>Vlr. unit.</strong></td>
                <td class="right"><strong>Vlr. dcto.</strong></td>
                <td class="right"><strong>Vlr. total</strong></td>
            </tr>

            @foreach($documento->factura as $f)
            <tr>
                <td>{{ $f->producto->id }} - {{ $f->producto->nombre }}</td>
                <td class="right">{{ $f->cantidad }}</td>
                <td class="right">{{ $f->descuento }}%</td>
                <td class="right">${{ $f->valor_unitario }}</td>
                <td class="right">${{ $f->valor_descuento }}</td>
                <td class="right">${{ $f->valor_total }}</td>
            </tr>
            @endforeach

            <tr>
                <td><strong>Totales</strong></td>
                <td class="right"><strong>{{ $documento->cantidad }}</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="right"><strong>${{ $documento->valor_descuento }}</strong></td>
                <td class="right"><strong>${{ $documento->valor_total }}</strong></td>
            </tr>
        </table>
    </div>
    
    {{--<h2>Detalle de servicios prestados</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr class='doble'>
                <td><strong>Empleado</strong></td>
                <td><strong>Cargo</strong></td>
                <td><strong>Servicio</strong></td>
            </tr>
        @foreach($documento->factura as $f)

        @if ($f->producto->tipo == "S" && sizeof($f->servicios))
            
            @foreach ($f->servicios as $s)
            <tr>
                <td>{{ $s->empleado->nombre }}</td>
                <td>{{ $s->cargo->nombre }}</td>
                <td>{{ $f->producto->id }} - {{ $f->producto->nombre }}</td>
            </tr>
            @endforeach

        @endif

        @endforeach
        </table>
    </div>--}}
    
    <h2>Trabajadores</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr class='doble'>
                <td><strong>Cedula</strong></td>
                <td><strong>Nombre</strong></td>
            </tr>
        @foreach($empleados as $e)
            <tr>
                <td>{{ $e['cedula'] }}</td>
                <td>{{ $e['nombre'] }}</td>
            </tr>
        @endforeach
        </table>
    </div>
    
    <h2>Medios de pago</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr class='doble'>
                <td><strong>Medio de pago</strong></td>
                <td class="right"><strong>Monto</strong></td>
            </tr>

            @foreach($documento->abonos as $a)
            <tr>
                <td>{{ $a->medioPago->nombre }}</td>
                <td class="right">${{ $a->valor }}</td>
            </tr>
            @endforeach
        </table>
    </div>
    
    @if (!empty($documento->observaciones))
    <h2>Observaciones</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td>{{ $documento->observaciones }}</td>
            </tr>
        </table>
    </div>
    @endif
@stop