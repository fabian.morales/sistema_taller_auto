<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('facturas/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">&Uacute;ltimos documentos</div>
</div>        
<div class="row item lista">
    <div class="small-3 columns">N&uacute;m</div>
    <div class="small-4 columns">Tipo</div>
    <div class="small-3 columns">Fecha</div>            
    <div class="small-2 columns">Opciones</div>            
</div>

@forelse($facturas as $f)
<div class="row item lista">
    <div class="small-3 columns">{{ $f->num }}</div>
    <div class="small-4 columns">{{ $f->tipoDocumento->sigla }}</div>
    <div class="small-3 columns">{{ $f->fecha }}</div>            
    <div class="small-2 columns">
        <a href="{{ url('/facturas/detalle/'.$f->id) }}" title="Ver Detalle"><i class="fi-magnifying-glass">&nbsp;</i></a>
        <a href="{{ url('/facturas/pdf/'.$f->id) }}" target='_blank' title="Ver en PDF"><i class="fi-page-pdf"></i>&nbsp;</a>
    </div>            
</div>
@empty
    <p class="text-center">No se encontr&oacute; documentos</p>
@endforelse

<div class="row">
    <div class="small-12 columns text-center">
        {!! $facturas->render() !!}
    </div>
</div>