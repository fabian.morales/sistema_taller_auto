@extends('master')

@section('content')
<div class="row titulo lista">
    <div class="small-12 columns">Detalle del documento</div>
</div> 
<fieldset>
    <legend>Documento</legend>
    <div class="row">
        <div class="small-3 columns"><strong>Tipo de documento: </strong></div>
        <div class="small-3 columns">{{ $documento->tipoDocumento->sigla }} - {{ $documento->tipoDocumento->nombre }}</div>
        <div class="small-3 columns"><strong>N&uacute;mero de documento: </strong></div>
        <div class="small-3 columns">{{ $documento->num }}</div>
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Cliente: </strong></div>
        <div class="small-9 columns">{{ $documento->cliente->id_nit }} - {{ $documento->cliente->nombres }}</div>
        <div class="clearfix"></div>
        @if(sizeof($documento->vehiculo))
        <div class="small-3 columns"><strong>Veh&iacute;culo: </strong></div>
        <div class="small-9 columns">{{ $documento->vehiculo->placa }} - {{ $documento->vehiculo->marca }} - {{ $documento->vehiculo->tipo->nombre }}</div>
        <div class="clearfix"></div>
        @endif
        <div class="small-3 columns"><strong>Fecha de creaci&oacute;n: </strong></div>
        <div class="small-3 columns">{{ $documento->fecha }}</div>
        <div class="small-3 columns"><strong>Creado por: </strong></div>
        <div class="small-3 columns">{{ $documento->usuarioCreacion->nombre }}</div>
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Cantidad items: </strong></div>
        <div class="small-3 columns">{{ $documento->cantidad }}</div>
        <div class="small-3 columns"><strong>Valor descuentos: </strong></div>
        <div class="small-3 columns">${{ $documento->valor_descuento }}</div>
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Valor total: </strong></div>
        <div class="small-3 columns end">${{ $documento->valor_total }}</div>
        
        @if(sizeof($documento->relacionado))
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Relacionado: </strong></div>
        <div class="small-9 columns">{{ $documento->relacionado->tipoDocumento->nombre . ' ' . $documento->relacionado->tipoDocumento->sigla . ' - ' . $documento->relacionado->num }}</div>
        @endif
    </div>
</fieldset>

<fieldset>
    <legend>Productos</legend>
    <div class="row item lista fn-small">
        <div class="small-4 columns"><strong>Producto</strong></div>
        <div class="small-1 columns"><strong>Cant.</strong></div>
        <div class="small-1 columns"><strong>Dcto.</strong></div>
        <div class="small-2 columns"><strong>Vlr. unit.</strong></div>
        <div class="small-2 columns"><strong>Vlr. dcto.</strong></div>            
        <div class="small-2 columns"><strong>Vlr. total</strong></div>
    </div>

    @foreach($documento->factura as $f)
    <div class="row item lista fn-small">
        <div class="small-4 columns">
            @if ($f->producto->tipo == "S")
            <a href="#serv_{{ $f->id }}" class="resalte" title="Haga clic para ver los empleados que prestaron el servicio" data-featherlight>{{ $f->producto->nombre }}&nbsp;<span class="fi-zoom-in"></span></a>
            @else
            {{ $f->producto->nombre }}
            @endif
        </div>            
        <div class="small-1 columns">{{ $f->cantidad }}</div>
        <div class="small-1 columns">{{ $f->descuento }}%</div>
        <div class="small-2 columns">${{ $f->valor_unitario }}</div>
        <div class="small-2 columns">${{ $f->valor_descuento }}</div>            
        <div class="small-2 columns">${{ $f->valor_total }}</div>
    </div>
    @if ($f->producto->tipo == "S" && sizeof($f->servicios))
    <div class="hide" id="serv_{{ $f->id }}">
        <div class="row">
            <div class="small-12 columns">
                <h3>Empleados que prestaron el servicio</h3>
            </div>
        </div>
        <div class="row item lista fn-small">            
            <div class="small-6 columns"><strong>Empleado</strong></div>            
            <div class="small-6 columns"><strong>Cargo</strong></div>
        </div>
        @foreach ($f->servicios as $s)
        <div class="row item lista fn-small">
            <div class="small-6 columns">{{ $s->empleado->nombre }}</div>
            <div class="small-6 columns">{{ $s->cargo->nombre }}</div>
        </div>
        @endforeach
    </div>
    @endif
    
    @endforeach
</fieldset>

<fieldset>
    <legend>Medios de pago</legend>
    <div class="row item lista fn-small">
        <div class="small-4 columns"><strong>Medio de pago</strong></div>            
        <div class="small-2 columns end"><strong>Monto</strong></div>
    </div>

    @foreach($documento->abonos as $a)
    <div class="row item lista fn-small">
        <div class="small-4 columns">{{ $a->medioPago->nombre }}</div>            
        <div class="small-2 columns end">${{ $a->valor }}</div>
    </div>
    @endforeach
</fieldset>

<fieldset>
    <legend>Otros</legend>
    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 columns"><strong>Observaciones</strong></div>
            <div class="small-12 columns">{{ $documento->observaciones }}</div>
        </div>
    </div>
</fieldset>

<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('/facturas/pdf/'.$documento->id) }}" class="button" target='_blank'><i class="fi-page-pdf"></i>&nbsp;Ver en PDF</a>
        <a href="{{ url('/facturas/') }}" class="button">Regresar</a>
    </div>
</div>
@stop
