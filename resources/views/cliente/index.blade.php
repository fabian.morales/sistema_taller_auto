@extends('master')
@section('js_header')
<script>
(function($, window){
    $(document).ready(function() {       
        $("#btnBuscarCli").click(function(e) {
            e.preventDefault();
            $("#loader").addClass("loading");
            $.ajax({
                url: '{{ asset("/clientes/lista") }}',
                method: 'post',
                data: $("#form_buscar_cli").serialize()
            })
            .done(function(res) {
                $("#div_clientes").html(res);
            })
            .always(function() {
                $("#loader").removeClass("loading");
            });
        });
    });
})(jQuery, window);
</script>
@stop

@section('content')
<fieldset>
    <legend>Buscador</legend>
    <form id="form_buscar_cli">
		<input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="row">
            <div class="small-12 medium-1 columns"><label for="cedula">C&eacute;dula</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="cedula" id="cedula" /></div>
            <div class="small-12 medium-1 columns"><label for="nombre">Nombre</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="nombre" id="nombre" /></div>
            <div class="small-12 medium-1 columns"><label for="apellidos">Apellidos</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="apellidos" id="apellidos" /></div>
            <div class="small-12 medium-2 columns end">
                <button class="tiny button rojo" id="btnBuscarCli">Buscar</button>
            </div>
        </div>
    </form>
</fieldset>
<div id="div_clientes">
@include('cliente.lista', array("clientes" => $clientes))
</div>
@stop