@extends('master')

@section('content')
<h2>Datos del cliente</h2>
<form id="form_cliente" name="form_cliente" action="{{ url('clientes/guardar') }}" method="post">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" name="id" value="{{ $cliente->id }}" />
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="id_nit">C&eacute;dula / Nit</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="id_nit" id="id_nit" value="{{ $cliente->id_nit }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombres">Nombres</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="nombres" id="nombres" value="{{ $cliente->nombres }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="apellidos">Apellidos</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="apellidos" id="apellidos" value="{{ $cliente->apellidos }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="direccion">Direcci&oacute;n</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="direccion" id="direccion" value="{{ $cliente->direccion }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="telefono">Tel&eacute;fono</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="telefono" id="telefono" value="{{ $cliente->telefono }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="email">Correo electr&oacute;nico</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="email" id="email" value="{{ $cliente->email }}" />
        </div>
    </div>
    @if(sizeof($cliente->vehiculos))
    <fieldset>
        <legend>Veh&iacute;culos asociados</legend>
        <div class="row item lista">
            <di class="medium-4 columns">Nombre</di>
            <di class="medium-3 columns">Marca</di>
            <di class="medium-3 columns">Tipo</di>
            <di class="medium-2 columns">Placa</di>
        </div>
        @foreach($cliente->vehiculos as $v)
        <div class="row item lista">
            <di class="medium-4 columns">{{ $v->nombre }}</di>
            <di class="medium-3 columns">{{ $v->marca }}</di>
            <di class="medium-3 columns">{{ $v->tipo->nombre }}</di>
            <di class="medium-2 columns">{{ $v->placa }}</di>
        </div>
        @endforeach
    </fieldset>
    @endif
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/clientes/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop