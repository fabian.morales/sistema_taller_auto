@extends('masterPdf')

@section('content')
{{--*/ $docDg = $documento->docDiagnostico->first()  /*--}}
<table cellspacing="5" cellpadding="0" class="sep">
    <tr>
        <td><strong>Documento</strong></td>
        <td>{{  $documento->tipoDocumento->nombre . ' ' . $documento->tipoDocumento->sigla }} - {{ $documento->num }}</td>
    </tr>
    <tr>
        <td><strong>Fecha:</strong></td>
        <td>{{ $documento->fecha }}</td>
    </tr>
    <tr>
        <td><strong>Cliente:</strong></td>
        <td>{{ $documento->cliente->id_nit }} - {{ $documento->cliente->nombres }}</td>
    </tr>
</table>

<h2>Datos del veh&iacute;culo</h2>
<div class='bordes sep'>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td><strong>Placa</strong></td>
            <td>{{ $documento->vehiculo->placa }}</td>
            <td><strong>Marca</strong></td>
            <td>{{ $documento->vehiculo->marca }}</td>
        </tr>
        <tr>
            <td><strong>Tipo</strong></td>
            <td colspan='3'>{{ $documento->vehiculo->tipo->nombre }}</td>
        </tr>
        <tr>
            <td><strong>Color</strong></td>
            <td>{{ $docDg->color }}</td>
            <td><strong>Motor</strong></td>
            <td>{{ $docDg->motor }}</td>
        </tr>
        <tr>
            <td><strong>Kilometraje</strong></td>
            <td>{{ $docDg->km }}</td>
            <td><strong>Cilindraje</strong></td>
            <td>{{ $docDg->cilindraje }}</td>
        </tr>
        <tr>
            <td><strong>Caja</strong></td>
            <td colspan="3">{{ $docDg->caja }}</td>
        </tr>
        <tr>
            <td><strong>Estado general</strong></td>
            <td colspan='3'>{{ $docDg->estado_general }}</td>
        </tr>
        <tr>
            <td><strong>Aval&uacute;o v&aacute;lido por diez (10) d&iacute;as</strong></td>
            <td colspan='3'>$ {{ $docDg->avaluo }}</td>
        </tr>
    </table>
</div>

@if (!empty($documento->observaciones))
<h2>Observaciones</h2>
<div class='bordes sep'>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td>{{ $documento->observaciones }}</td>
        </tr>
    </table>
</div>
@endif

<h2>Revisi&oacute;n de partes</h2>
<div class='bordes sep'>
    <table cellspacing="0" cellpadding="0">
        <tr class='doble'>
            <td style='width: 27%;'><strong>Parte</strong></td>
            <td style='width: 5%;' class="right"><strong>Cantidad</strong></td>
            @foreach ($opciones as $o)
            <td style='width: 7%;' class="center"><strong>{{ $o->nombre }}</strong></td>
            @endforeach
            <td style='width: 40%;'><strong>Observaciones</strong></td>
        </tr>

        @foreach($categorias as $c)
        <tr>
            <td colspan="10"><strong>{{ $c->nombre }}</strong></td>
        </tr>

        @foreach($c->items as $i)
        
        @if(sizeof($i->diagnosticos))
        <tr>
            <td>{{ $i->nombre }}</td>
            <td class="right">@if($i->requiere_cant == 'Y') {{ $i->diagnosticos[0]->cantidad . ' ' . $i->unm }} @endif</td>
            @foreach ($opciones as $o)
            <td class="center">@if($i->diagnosticos[0]->id_opc_dg == $o->id) X @endif</td>
            @endforeach
            <td>{{ $i->diagnosticos[0]->observaciones }}</td>
        </tr>        
        @endif
        
        @endforeach

        @endforeach
    </table>
</div>

@if (sizeof($docDg->imagenes))
<h2>Im&aacute;genes</h2>

@foreach($docDg->imagenes as $i)
<div class='imagen'>
    <img src='{{ public_path('storage/imagenes/'.$documento->tipoDocumento->sigla.'/'.$documento->id).'/'.$i->id.'.'.$i->archivo }}' />
</div>
@endforeach

@endif
<div style="clear: both"></div>
@if (sizeof($docDg->partes))
<h2>Zonas resaltadas del veh&iacute;culo</h2>

<ul>
    @foreach($docDg->partes as $p)
    <li>{{ $p->parteVehiculo->nombre }}</li>
    @endforeach
</ul>
@endif

<div style="width: 100%; height: 70px;"></div>

<div>
    IMPORTANTE: En este peritaje se efectuaron las respectivas pruebas externas de motor, caja, transmisi&oacute;n, partes
    el&eacute;ctricas, etc. y su funcionamiento es acorde a lo detallado en &eacute;l, y para nada compromete a la compa&ntilde;&iacute;a en el
    estado interno de estas partes ya que &eacute;stas no se desarman. Este peritaje tiene vigencia por el d&iacute;a de su
    elaboraci&oacute;n
</div>

<div style="width: 100%; height: 150px;"></div>

<div style="width: 25%; float: left; margin-right: 50px; line-height: 40px; font-weight: bold; text-align: center; border-top: 2px solid #000">
    Avaluado por
</div>

<div style="width: 25%; float: left; margin-right: 50px; line-height: 40px; font-weight: bold; text-align: center; border-top: 2px solid #000">
    Firma cliente
</div>

@stop