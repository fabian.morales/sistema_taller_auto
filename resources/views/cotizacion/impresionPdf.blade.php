@extends('masterPdf')

@section('content')
    <table cellspacing="5" cellpadding="0" class="sep">
        <tr>
            <td><strong>Documento</strong></td>
            <td>{{  $documento->tipoDocumento->nombre . ' ' . $documento->tipoDocumento->sigla }} - {{ $documento->num }}</td>
        </tr>
        <tr>
            <td><strong>Fecha:</strong></td>
            <td>{{ $documento->fecha }}</td>
        </tr>
        <tr>
            <td><strong>Cliente:</strong></td>
            <td>{{ $documento->cliente->id_nit }} - {{ $documento->cliente->nombres }}</td>
        </tr>
    </table>

    @if(sizeof($documento->vehiculo))
    <h2>Datos del veh&iacute;culo</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td><strong>Placa</strong></td>
                <td>{{ $documento->vehiculo->placa }}</td>
                <td><strong>Marca</strong></td>
                <td>{{ $documento->vehiculo->marca }}</td>
            </tr>
            <tr>
                <td><strong>Tipo</strong></td>
                <td colspan='3'>{{ $documento->vehiculo->tipo->nombre }}</td>
            </tr>
            <tr>
                <td><strong>Color</strong></td>
                <td>{{ $documento->vehiculo->color }}</td>
                <td><strong>Motor</strong></td>
                <td>{{ $documento->vehiculo->motor }}</td>
            </tr>
            <tr>
                <td><strong>Kilometraje</strong></td>
                <td>{{ $documento->vehiculo->km }}</td>
                <td><strong>Cilindraje</strong></td>
                <td>{{ $documento->vehiculo->cilindraje }}</td>
            </tr>
            <tr>
                <td><strong>Caja</strong></td>
                <td colspan="3">{{ $documento->vehiculo->caja }}</td>
            </tr>
        </table>
    </div>
    @endif
    
    <h2>Productos y servicios</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr class='doble'>
                <td><strong>Producto</strong></td>
                <td class="right"><strong>Cant.</strong></td>
                <td class="right"><strong>Dcto.</strong></td>
                <td class="right"><strong>Vlr. unit.</strong></td>
                <td class="right"><strong>Vlr. dcto.</strong></td>
                <td class="right"><strong>Vlr. total</strong></td>
            </tr>

            @foreach($documento->cotizacion as $c)
            <tr>
                <td>{{ $c->producto->id }} - {{ $c->producto->nombre }}</td>
                <td class="right">{{ $c->cantidad }}</td>
                <td class="right">{{ $c->descuento }}%</td>
                <td class="right">${{ $c->valor_unitario }}</td>
                <td class="right">${{ $c->valor_descuento }}</td>
                <td class="right">${{ $c->valor_total }}</td>
            </tr>
            @endforeach

            <tr>
                <td><strong>Totales</strong></td>
                <td class="right"><strong>{{ $documento->cantidad }}</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="right"><strong>${{ $documento->valor_descuento }}</strong></td>
                <td class="right"><strong>${{ $documento->valor_total }}</strong></td>
            </tr>
        </table>
    </div>
    
    @if (!empty($documento->observaciones))
    <h2>Observaciones</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td>{{ $documento->observaciones }}</td>
            </tr>
        </table>
    </div>
    @endif
@stop