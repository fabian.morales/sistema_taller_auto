<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('vehiculos/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de veh&iacute;culos</div>
</div>
<div class="row item lista">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-3 columns">Nombre</div>
    <div class="small-2 columns">Placa</div>
    <div class="small-3 columns">Propietario</div>
    <div class="small-2 columns">Editar</div>
</div>
@foreach($vehiculos as $v)
<div class="row item lista">
    <div class="small-2 columns">{{ $v->id }}</div>
    <div class="small-3 columns">{{ $v->nombre }}</div>
    <div class="small-2 columns">{{ $v->placa }}</div>
    <div class="small-3 columns">@if(sizeof($v->propietario)){{ $v->propietario->nombres }} {{ $v->propietario->apellidos }}@endif</div>
    <div class="small-2 columns">
        <a href="{{ url('/vehiculos/editar/'.$v->id) }}" title="Editar vehiculo"><i class="fi-pencil"></i></a>
    </div>
</div>        
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $vehiculos->render() !!}
    </div>
</div>