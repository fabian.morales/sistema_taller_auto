<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <title>PDF</title>
        <style type='text/css'>
            body, html{
                font-family: 'helvetica';
                font-size: 12px;
            }
            
            hr {
                display: block;
                height: 1px;
                border: 0;
                border-top: 1px solid #ccc;
                margin: 1em 0;
                padding: 0; 
            }
            
            .bordes{
                border: 1px solid #333;
                border-radius: 5px;
                padding: 0;
            }
            
            .bordes table{
                width: 100%;
                /*border: 1px solid #333;*/
            }
            
            .bordes table td{
                padding: 5px;
                border: 1px solid #333;
            }
            
            .bordes table tr td:first-child{
                border-left: none;
            }
            
            .bordes table tr td:last-child{
                border-right: none;
            }
            
            .bordes table tr:first-child td{
                border-top: none;
            }
            
            .bordes table tr:last-child td{
                border-bottom: none;
            }
            
            table.sep{
                margin-bottom: 20px;
            }
            
            tr.doble td{
                border-bottom: 4px double #333 !important;
            }
            
            .center{
                text-align: center;
            }
            
            .right{
                text-align: right;
            }
            
            .bordes.sep{
                margin-bottom: 10px;
            }
            
            .sub_enc{
                text-transform: uppercase;
                color: #555;
            }
            
            .imagen{
                margin-right: 5px;
                width: 20%;
                height: 200px;
                overflow: hidden;
                display: block;
                float: left;
            }
            
            .imagen img{
                width: 100%;
            }
            
            img.icono{
                width: 17px;
                height: 17px;
            }
        </style>
    </head>
<body>
    <script type="text/php">
        if ( isset($pdf) ) {
            $font = Font_Metrics::get_font('helvetica', 'normal');
            $size = 9;
            $y = $pdf->get_height() - 30;
            $x = ($pdf->get_width() / 2) - Font_Metrics::get_text_width('1/1', $font, $size);
            $pdf->page_text($x, $y, '{PAGE_NUM} de {PAGE_COUNT}', $font, $size);
        }
    </script>
    <table>
        <tr>
            <td colspan="2"><img src="{{ public_path('img').'/titulo.png' }}" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <span>Mec&aacute;nica  en General - L&aacute;mina y Pintura - An&aacute;lisis de Gases</span><br />
                <span><strong><big>Leonardo Fabio R&iacute;os S&aacute;nchez</big></strong></span><br />
                <span class='sub_enc'>Régimen simplificado Nit 16.787.345-1</span><br /><br />
            </td>
        </tr>
        <tr>
            <td>
                <span class='sub_enc'><small>Calle 8 N 10-108 San Bosco</small></span><br />
                <span class='sub_enc'><small>Tel. 896 00 38</small></span><br />
                <span class='sub_enc'><small>Cels. 316 627 81 81</small></span><br />
            </td>
            <td>
                <span class='sub_enc'><small>leorisan@hotmail.com</small></span><br />
                <span class='sub_enc'><img class="icono" src="{{ public_path('img').'/whatsapp.png' }}" style="width: 12px; height: 12px; display: inline-block; margin-top: 1px;" />&nbsp;<small>316 627 8181</small></span>
            </td>
        </tr>
    </table>
    <hr />
    @yield('content')
</body>
</html>