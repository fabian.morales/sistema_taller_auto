<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <title>Diagnosticentro</title>
        <link href="{{ asset('foundation/css/foundation.css') }}" rel="stylesheet" />
        <link href="{{ asset('foundation/fonts/foundation-icons.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/featherlight/featherlight.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/slider/jquery-ui-slider-pips.css') }}" rel="stylesheet" />
        <link href="{{ asset('js/filer/css/jquery.filer.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />
        @section ('css_header')
        @show

        <script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
        <script src="{{ asset('foundation/js/foundation.min.js') }}"></script>
        <script src="{{ asset('foundation/js/vendor/modernizr.js') }}"></script>
        <script src="{{ asset('js/featherlight/featherlight.js') }}"></script>
        <script src="{{ asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('js/slider/jquery-ui-slider-pips.js') }}"></script>
        <!--script src="{{ asset('js/jquery.rwdImageMaps.min.js') }}"></script-->
        <script src="{{ asset('js/jquery.imagemapster.min.js') }}"></script>
        <script src="{{ asset('js/filer/js/jquery.filer.min.js') }}"></script>
        <script src="{{ asset('js/lodash.js') }}"></script>
        <script src="{{ asset('js/script.js') }}"></script>
        <!--script src="{{ asset('js/jquery-ui/jquery-ui.js') }}"></script>
        <!--script src="{{ asset('js/jquery.validate/jquery.validate.js') }}"></script>
        <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
        <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>
        <script src="{{ asset('js/gumby.min.js') }}"></script>
        <script src="{{ asset('js/site.js') }}" language="javascript"></script-->
        <script>
$(document).foundation();
(function (window, $) {
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).foundation();
    });
})(window, jQuery);
        </script>
        @section ('js_header')
        @show
    </head>
    <body>
        <div id="loader"><div></div></div>
        <div class="row">
            <div class="small-12 columns">
                <div class="mensajes">
                    @if (Session::has('mensajeError'))
                    <div data-alert class="alert-box alert radius">
                        {{ Session::get('mensajeError') }}
                        <a href="#" class="close">&times;</a>
                    </div>
                    @endif
                    @if (Session::has('mensaje'))
                    <div data-alert class="alert-box success radius">
                        {{ Session::get('mensaje') }}
                        <a href="#" class="close">&times;</a>
                    </div>
                    @endif
                    @if (Session::has('mensajeExt'))
                    {{ Session::get('mensajeExt') }}
                    @endif
                </div>
            </div>
        </div>
        @if (Auth::check())
        <div class="row sup">
            <div class="small-12 medium-6 end columns">
                <a href="{{ url("/") }}">
                    <img src="{{ asset('img/titulo.png') }}" />
                </a>
            </div>
        </div>
        <div class="row sup">
            <div class="small-12 columns">
                <nav class="right">
                    <ul>
                        <li>
                            <span class="padre">Cat&aacute;logo e Inventarios <i class="fi-arrow-down"></i></span>
                            <ul>
                                <li><a href="{{ url('/productos/') }}">Cat&aacute;logo</a></li>
                                <li><a href="{{ url('/movimientos/') }}">Movimientos de inventario</a></li>
                                <li><a href="{{ url('/productos/existencias') }}">Ver existencias</a></li>
                            </ul>
                        </li>
                        <li>
                            <span class="padre">Cuentas de cobro <i class="fi-arrow-down"></i></span>
                            <ul>
                                <li><a href="{{ url('/clientes/') }}">Clientes</a></li>
                                <li><a href="{{ url('/mediospago/') }}">Medios de pago</a></li>
                                <li><a href="{{ url('/facturas/') }}">Documentos</a></li>
                                <li><a href="{{ url('/cotizacion/') }}">Cotizaciones</a></li>
                            </ul>
                        </li>
                        <li>
                            <span class="padre">Ordenes de entrada <i class="fi-arrow-down"></i></span>
                            <ul>
                                <li><a href="{{ url('/diagnostico/') }}">Categor&iacute;as e &iacute;tems de diagn&oacute;stico</a></li>
                                <li>
                                    <span class="padre">Veh&iacute;culos <i class="fi-arrow-down"></i></span>
                                    <ul>
                                        <li><a href="{{ url('/vehiculos/partes/') }}">Partes de veh&iacute;culos</a></li>
                                        <li><a href="{{ url('/vehiculos/tipos/') }}">Tipos de veh&iacute;culos</a></li>
                                        <li><a href="{{ url('/vehiculos/') }}">Gesti&oacute;n de veh&iacute;culos</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/orden/entrada/') }}">&Oacute;rdenes de entrada</a></li>
                                <li><a href="{{ url('/orden/servicio/') }}">&Oacute;rdenes de servicio</a></li>
                                <li><a href="{{ url('/orden/peritaje/') }}">Peritajes</a></li>
                            </ul>
                        </li>
                        <li>
                            <span class="padre">N&oacute;mina <i class="fi-arrow-down"></i></span>
                            <ul>
                                <li><a href="{{ url('/cargos/') }}">Cargos</a></li>
                                <li><a href="{{ url('/usuarios/') }}">Empleados</a></li>
                                <li><a href="{{ url('/nomina/periodo/') }}">Periodos</a></li>
                            </ul>
                        </li>
                        <!--li>Reportes</li-->
                    </ul>
                </nav>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="small-12 columns cont_form">
                <div class="row">
                    <div class="small-12 columns">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
