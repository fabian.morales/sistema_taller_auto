@extends('master')

@section('js_header')
<script>
(function($, window){
    $(document).ready(function() {
        var $datePickerOpc = {
            dateFormat: "yy-mm-dd",
            numberOfMonths: 1,
            dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "Mau", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            currentText: "Hoy",
            closeText: "Aceptar",
            maxPicks: 1,
            showButtonPanel: true
        };
        
        $("#fecha_inicio, #fecha_fin").datepicker($datePickerOpc);
        
        $("#btnBuscarOrden").click(function(e) {
            e.preventDefault();
            $("#loader").addClass("loading");
            $.ajax({
                url: '{{ asset("/orden/entrada/lista") }}',
                method: 'post',
                data: $("#form_buscar_orden").serialize()
            })
            .done(function(res) {
                $("#divordenes").html(res);
            })
            .always(function() {
                $("#loader").removeClass("loading");
            });
        });
        
        $("#btnLimpiar").click(function(e) {
            e.preventDefault();
            $("#form_buscar_orden input").each(function(i, o) {
                $(o).val('');
            });
            
            $("#btnBuscarOrden").click();
        });
    });
})(jQuery, window);
</script>
@stop

@section('content')
<fieldset>
    <legend>Buscador</legend>
    <form id="form_buscar_orden">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="row">
            <div class="small-12 medium-1 columns"><label for="fecha_inicio">Fecha inicio</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="fecha_inicio" id="fecha_inicio" /></div>
            <div class="small-12 medium-1 columns"><label for="fecha_fin">Fecha fin</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="fecha_fin" id="fecha_fin" /></div>
            <div class="small-12 medium-1 columns"><label for="cedula">C&eacute;dula</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="cedula" id="cedula" /></div>
            <div class="small-12 medium-1 columns"><label for="placa">Placa</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="placa" id="placa" /></div>
            <div class="small-12 medium-3 columns end">
                <button class="tiny button rojo" id="btnBuscarOrden">Buscar</button>
                <button class="tiny button left" id="btnLimpiar">Limpiar</button>
            </div>
        </div>
    </form>
</fieldset>
<div id="divordenes">
@include('ordenentrada.lista', array("ordenes" => $ordenes))
</div>
@stop