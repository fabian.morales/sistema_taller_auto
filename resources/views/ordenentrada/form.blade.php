@extends('master')

@section('js_header')
<script>
    (function ($, window) {
        $(window).bind("beforeunload", function() {
            return "¿Está seguro de querer salir?";
        });
        
        $(document).ready(function () {
            var niveles = ["Bajo", "1/4", "Medio", "3/4", "Alto"];

            $("#btnBuscarCliente").click(function (e) {
                e.preventDefault();
                $("#loader").addClass("loading");
                $.ajax({
                    url: '{{ url("/clientes/buscar") }}',
                    method: 'post',
                    data: {'cedula': $("#cedula").val(), _token: '{!! csrf_token() !!}'},
                    dataType: 'json',
                    success: function (json) {
                        if (!_.isEmpty(json)) {
                            $("#nombre").val(json.nombres);
                            $("#apellidos").val(json.apellidos);
                            $("#direccion").val(json.direccion);
                            $("#telefono").val(json.telefono);

                            $("#id_vehiculo").html('');
                            $("#id_vehiculo").append('<option id=""></option>');
                            $.each(json.vehiculos, function (i, o) {
                                $("#id_vehiculo").append('<option value="' + o.id + '">' + o.nombre + ' - ' + o.placa + '</option>');
                            });

                            //$jsonDocumento.cedulaCliente =  $("#cedula").val();
                            //enviarSesion();
                        } else {
                            alert('Cliente no encontrado');
                        }

                        $("#loader").removeClass("loading");
                    }
                });
            });

            $("#sel_nivel_combustible").slider({
                min: 0,
                max: niveles.length - 1,
                value: @if(sizeof($documento->docDiagnostico)) {{ $documento->docDiagnostico[0]->nivel_combustible }} @else 2 @endif
            })
            .slider("pips", {
                rest: "label",
                labels: niveles
            })
            .on("slidechange", function (e, ui) {
                $("#nivel_combustible").val(ui.value);
            });

            $("#id_vehiculo").change(function (e) {
                e.preventDefault();
                $("#loader").addClass("loading");
                $.ajax({
                    url: '{{ url("/orden/entrada/buscarVehiculo") }}/' + $(this).val(),
                    method: 'post',
                    data: {_token: '{!! csrf_token() !!}'},
                    dataType: 'json',
                    success: function (json) {
                        if (!_.isEmpty(json)) {
                            $("#marca").val(json.marca);
                            $("#motor").val(json.motor);
                            $("#serie").val(json.serie);
                            $("#color").val(json.color);
                            $("#placa").val(json.placa);
                            $("#km").val(json.km);
                            $("#cilindraje").val(json.cilindraje);
                            $("#caja").val(json.caja);

                            var $areas = [];
                            $("#mapa_carro").html('');
                            $("#datos_mapa").html('');
                            $.each(json.tipo.partes, function (i, o) {
                                $("#mapa_carro").append('<area alt="' + o.nombre + '" title="' + o.nombre + '" href="#" shape="poly" name="' + o.llave + '" coords="' + o.pivot.path + '" data-id="' + o.id + '" />');
                                $("#datos_mapa").append('<input type="hidden" id="dato_' + o.llave + '" name="dato_mapa[' + o.id + ']" value="" />');
                                $areas.push({key: o.llave, fillColor: "ff0000"});
                            });

                            $("#img_mapa_carro").attr("src", "{{ asset('/img/mapas/') }}/tipo_" + json.tipo.id + ".jpg").css("display", "block");

                            $('#img_mapa_carro').mapster({
                                fillOpacity: 0.4,
                                fillColor: "ff0000",
                                stroke: true,
                                strokeColor: "333333",
                                strokeOpacity: 0.8,
                                strokeWidth: 1,
                                singleSelect: false,
                                mapKey: 'name',
                                listKey: 'name',
                                onClick: function (e) {
                                    var $id = $(this).attr("name");
                                    $("#dato_" + $id).val(e.selected === true ? 1 : 0);
                                    /*$(this).mapster('select');*/
                                },
                                //showToolTip: true,
                                //toolTipClose: ["tooltip-click", "area-click"],
                                areas: $areas
                            });

                            @if(sizeof($documento->docDiagnostico) && sizeof($documento->docDiagnostico[0]->partes))

                            @foreach($documento->docDiagnostico[0]->partes as $p)
                            $('#img_mapa_carro').mapster('set', true, '{{ $p->parteVehiculo->llave }}');
                            @endforeach

                            @endif

                            //$jsonDocumento.cedulaCliente =  $("#cedula").val();
                            //enviarSesion();
                        } else {
                            alert('Vehículo no encontrado');
                        }

                        $("#loader").removeClass("loading");
                    }
                });
            });
            
            @if(sizeof($documento->docDiagnostico))
            $("#id_vehiculo").change();           
            @endif

            $('#imagenes').filer({
                captions: {
                    button: "Seleccionar los archivos",
                    feedback: "Seleccione los archivos a subir",
                    feedback2: "archivos fueron seleccionados",
                    drop: "Arrastre el archivo aqui",
                    removeConfirmation: "¿Está seguro de borrar este archivo?",
                    errors: {
                        filesLimit: "Solo se permite subir hasta @{{fi-limit}} archivos.",
                        filesType: "Solo se permite subir imagenes.",
                        filesSize: "@{{fi-name}} es demasiado grande. Por favor suba archivos de hasta @{{fi-maxSize}} MB.",
                        filesSizeAll: "Los archivos selccionados son demasiado grandes. Por favor suba archivos de hasta @{{fi-maxSize}} MB."
                    }
                },
                showThumbs: true,
                templates: {
                    box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                    item: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="@{{fi-name}}">@{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">@{{fi-size2}}</span>\
                                        </div>\
                                        @{{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>@{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                    itemAppend: '<li class="jFiler-item">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-info">\
                                                <span class="jFiler-item-title"><b title="@{{fi-name}}">@{{fi-name | limitTo: 25}}</b></span>\
                                                <span class="jFiler-item-others">@{{fi-size2}}</span>\
                                            </div>\
                                            @{{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">@{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                    progressBar: '<div class="bar"></div>',
                    itemAppendToEnd: false,
                    removeConfirmation: true,
                    _selectors: {
                        list: '.jFiler-items-list',
                        item: '.jFiler-item',
                        progressBar: '.bar',
                        remove: '.jFiler-item-trash-action'
                    }
                }
            });

            $("#btnGuardarDocumento").click(function (e) {
                e.preventDefault();
                if ($("#cedula").val() === '') {
                    alert('Debe ingresar la cédula del cliente');
                    return;
                }

                if ($("#nombre").val() === '') {
                    alert('Debe ingresar el nombre del cliente');
                    return;
                }

                if ($("#apellidos").val() === '') {
                    alert('Debe ingresar los apellidos del cliente');
                    return;
                }

                if ($("#id_vehiculo").val() === '') {
                    alert('Debe seleccionar el vehículo');
                    return;
                }

                if ($("#marca").val() === '') {
                    alert('Debe ingresar la marca del vehículo');
                    return;
                }

                if ($("#motor").val() === '') {
                    alert('Debe ingresar el tipo de motor del vehículo');
                    return;
                }

                if ($("#color").val() === '') {
                    alert('Debe ingresar el color del vehículo');
                    return;
                }

                if ($("#serie").val() === '') {
                    alert('Debe ingresar la serie del vehículo');
                    return;
                }

                if ($("#placa").val() === '') {
                    alert('Debe ingresar la placa del vehículo');
                    return;
                }

                if ($("#km").val() === '') {
                    alert('Debe ingresar el kilometraje del vehículo');
                    return;
                }

                if ($("#cilindraje").val() === '') {
                    alert('Debe ingresar el cilindraje del vehículo');
                    return;
                }

                if ($("#caja").val() === '') {
                    alert('Debe ingresar el tipo de caja del vehículo');
                    return;
                }

                if ($("#observaciones").val() === '') {
                    if (!confirm('No ha ingresado las observaciones, ¿desea continuar?')) {
                        return;
                    }
                }

                if ($("#estado_general").val() === '') {
                    if (!confirm('No ha ingresado el estado general del vehículo, ¿desea continuar?')) {
                        return;
                    }
                }

                if (!confirm('¿Está seguro de guardar este documento?')) {
                    return;
                }

                $("#loader").addClass("loading");

                var $form = new FormData($('#form_orden')[0]);
                $.ajax({
                    url: '{{ url("orden/entrada/guardar") }}',
                    data: $form,
                    method: 'post',
                    cache: false,
                    contentType: false,
                    processData: false
                })
                .done(function (res) {
                    alert('La orden de entrada ha sido guardada exitosamente');
                    window.location.reload();
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseJSON.error.message);
                })
                .always(function () {
                    $("#loader").removeClass("loading");
                });
            });
        });
    })(jQuery, window);
</script>
@stop

@section('content')
<h2>Realizar una orden de entrada</h2>
<form id="form_orden" name="form_orden" action="{{ url('orden/entrada/guardar') }}" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" name="id" id="id" value="{{ $documento->id }}" />
    <input type="hidden" name="id_docdg" id="id_docdg" value="@if(sizeof($documento->docDiagnostico)){{ $documento->docDiagnostico[0]->id }}@endif" />
    <div id="datos_mapa"></div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Tipo</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select id="id_tipo" name="id_tipo">
                @foreach($tipos as $t)
                <option value="{{ $t->id }}" @if($t->id == $documento->id_tipo) selected @endif>{{ $t->sigla }} - {{ $t->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label for="estado">Documento cerrado
                    <input type="checkbox" name="estado" id="estado" value="C" @if(sizeof($documento->docDiagnostico) && $documento->docDiagnostico[0]->estado == "C") checked="checked" @endif />
                </label>
            </div>
        </div>
    </div>

    <fieldset>
        <legend>Datos de veh&iacute;culo</legend>
        <div class="row">
            <div class="small-12 medium-2 columns"><label for="cedula">C&eacute;dula</label></div>
            <div class="small-12 medium-4 columns">
                <div class="row collapse">
                    <div class="small-9 columns"><input type="text" id="cedula" name="cedula" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->id_nit }}" @endif/></div>
                    <div class="small-3 columns"><button class="button default rojo tiny" id="btnBuscarCliente">Buscar</button></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="nombre">Nombre propietario</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="nombre" name="nombre" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->nombres }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="apellido">Apellido propietario</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="apellidos" name="apellidos" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->apellidos }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="telefono">Tel&eacute;fono</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="telefono" name="telefono" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->telefono }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="direccion">Direcci&oacute;n</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="direccion" name="direccion" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->direccion }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="vehiculo">Veh&iacute;culo</label></div>
            <div class="small-12 medium-4 columns end">
                <select id="id_vehiculo" name="id_vehiculo">
                    @if(sizeof($documento->cliente) && sizeof($documento->cliente->vehiculos))
                    
                    @foreach($documento->cliente->vehiculos as $v)
                    <option value="{{ $v->id }}" @if($v->id == $documento->id_vehiculo) selected @endif>{{ $v->placa }} - {{ $v->nombre }}</option>
                    @endforeach
                    
                    @endif
                </select>
            </div>
            <div class="small-12 medium-2 columns"><label for="marca">Marca</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="marca" name="marca" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->marca }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="motor">Motor</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="motor" name="motor" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->motor }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="serie">Serie</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="serie" name="serie" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->serie }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="color">Color</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="color" name="color" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->color }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="placa">Placa</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="placa" name="placa" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->placa }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="km">Km</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="km" name="km" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->km }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="cilindraje">Cil. motor</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="cilindraje" name="cilindraje" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->cilindraje }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="caja">Caja</label></div>
            <div class="small-12 medium-4 columns end"><input type="text" id="caja" name="caja" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->caja }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="observaciones">Observaciones</label></div>
            <div class="small-12 medium-10 columns"><textarea id="observaciones" name="observaciones">{{ $documento->observaciones }}</textarea></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="estado_general">Estado general</label></div>
            <div class="small-12 medium-10 columns"><input type="text" id="estado_general" name="estado_general" value="@if(sizeof($documento->docDiagnostico)){{ $documento->docDiagnostico[0]->estado_general }}@endif" /></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="nivel_combustible">Nivel de combustible</label></div>
            <div class="small-12 medium-10 columns">
                <input type="hidden" id="nivel_combustible" name="nivel_combustible" value="@if(sizeof($documento->docDiagnostico)){{ $documento->docDiagnostico[0]->nivel_combustible }}@endif" />
                <div id="sel_nivel_combustible"></div>
            </div>
        </div>
    </fieldset>
    
    <fieldset>
        <legend>Diagn&oacute;stico inicial</legend>
        <textarea id="dg_inicial" name="dg_inicial" rows="10">@if(sizeof($documento->docDiagnostico)){{ $documento->docDiagnostico[0]->dg_inicial }}@endif</textarea>
    </fieldset>

    @foreach($categorias as $c)
    <fieldset>
        <legend>{{ $c->nombre }}</legend>
        <div class="row">
            <div class="small-4 columns"><strong>Revisi&oacute;n de</strong></div>
            <div class="small-2 columns"><strong>Cnt</strong></div>
            <div class="small-4 columns text-center">
                <strong>Estado</strong>
                <div class="row">
                    @foreach($opciones as $o)
                    <div class="small-3 columns">{{ $o->nombre }}</div>
                    @endforeach
                </div>
            </div>
            <div class="small-2 columns"><strong>Observaciones</strong></div>
        </div>
        <hr />
        @foreach($c->items as $i)
        <div class="row separador_gris">
            <div class="small-4 columns">
                {{ $i->nombre }}
            </div>
            <div class="small-2 columns">
                @if ($i->requiere_cant == "Y")
                <div class="row">
                    <div class="small-8 columns"><input type="text" name="cant_item[{{ $i->id }}]" id="cant_item_{{ $i->id }}" class="caja reducida" @if(sizeof($i->diagnosticos)) value="{{ $i->diagnosticos[0]->cantidad }}" @endif /></div>
                    <div class="small-4 columns"><span>{{ $i->unm }}</span></div>
                </div>
                @else
                &nbsp;
                @endif
            </div>
            <div class="small-4 columns text-center">
                <div class="row">
                    @foreach($opciones as $o)
                    <div class="small-3 columns"><input type="radio" id="resp_item_{{ $i->id }}_opc{{ $o->id }}" name="resp[{{ $i->id }}]" value="{{ $o->id }}" @if(sizeof($i->diagnosticos) && $i->diagnosticos[0]->id_opc_dg == $o->id) checked @endif /></div>
                    @endforeach
                </div>
            </div>
            <div class="small-2 columns"><input type="text" name="observaciones_item[{{ $i->id }}]" id="observaciones_item_{{ $i->id }}" class="caja reducida" @if(sizeof($i->diagnosticos)) value="{{ $i->diagnosticos[0]->observaciones }}" @endif /></div>
        </div>
        @endforeach
    </fieldset>
    @endforeach

    <div class="row">
        <div class="small-12 columns">
            <img src="" alt="Mapa carro" usemap="#mapa_carro" id="img_mapa_carro" style="display: none;" />
            <map name="mapa_carro" id="mapa_carro">

            </map>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <input type="file" name="imagenes[]" id="imagenes" multiple="multiple" />
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns text-right">
            <a class="button gris" href="{{ url('/orden/entrada/') }}" />Cancelar</a>
            <input type="button" value="Guardar" class="button naranja" id="btnGuardarDocumento" />
        </div>
    </div>
</form>
@stop