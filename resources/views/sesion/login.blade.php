<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Diagnosticentro</title>
    <link href="{{ asset('foundation/css/foundation.css') }}" rel="stylesheet" />
    <link href="{{ asset('foundation/fonts/foundation-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/featherlight/featherlight.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />
    @section ('css_header')
    @show
    
    <script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>    
    <script src="{{ asset('foundation/js/foundation.min.js') }}"></script>    
    <script src="{{ asset('foundation/js/vendor/modernizr.js') }}"></script>    
    <script src="{{ asset('js/featherlight/featherlight.js') }}"></script>
    <!--script src="{{ asset('js/jquery-ui/jquery-ui.js') }}"></script>
    <!--script src="{{ asset('js/jquery.validate/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>
    <script src="{{ asset('js/gumby.min.js') }}"></script>    
    <script src="{{ asset('js/site.js') }}" language="javascript"></script-->
    <script>
        $(document).foundation();
        (function (window, $){
            $(document).ready(function() {        
                $(document).foundation();
            });
        })(window, jQuery);          
    </script>
    @section ('js_header')
    @show
</head>
<body>
    <div id="div_loading"></div>       
    <div class="row">
        <div class="small-12 columns">
            <div class="mensajes">
                @if (Session::has('mensajeError'))            
                <div data-alert class="alert-box alert radius">
                    {{ Session::get('mensajeError') }}
                    <a href="#" class="close">&times;</a>
                </div>            
                @endif
                @if (Session::has('mensaje'))
                <div data-alert class="alert-box success radius">
                    {{ Session::get('mensaje') }}
                    <a href="#" class="close">&times;</a>
                </div>
                @endif 
                @if (Session::has('mensajeExt'))            
                    {{ Session::get('mensajeExt') }}            
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <form id="form_login" name="form_login" action="{{ url('/sesion/login') }}" method="post">
				<input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="row">
                    <div class="small-12 columns text-center">
                        <img src="{{ asset('img/titulo.png') }}" />
                        <hr />
                    </div>
                </div>
                <div class="row">
                    <div class="large-5 medium-6 small-12 columns large-centered medium-centered cont_form">
                        <div class="row collapse">
                            <div class="small-2 columns">
                                <strong class="uppercase">Usuario</strong>
                            </div>
                            <div class="small-10 columns">
                                <input type="text" name="login" id="login" placeholder="Nombre de usuario" />    
                            </div>
                        </div>
                        <div class="row collapse">
                            <div class="small-2 columns">
                                <strong class="uppercase">Clave</strong>
                            </div>
                            <div class="small-10 columns">
                                <input type="password" name="password" id="password" placeholder="Clave" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <input type="submit" value="Ingresar" class="button medium default right rojo" />
                            </div>
                        </div>            
                    </div>
                </div>    
            </form>
        </div>
    </div>
</body>
</html>