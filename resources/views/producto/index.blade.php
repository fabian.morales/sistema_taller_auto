@extends('master')
@section('js_header')
<script>
(function($, window){
    $(document).ready(function() {       
        $("#btnBuscarProd").click(function(e) {
            e.preventDefault();
            $("#loader").addClass("loading");
            $.ajax({
                url: '{{ asset("/productos/lista") }}',
                method: 'post',
                data: $("#form_buscar_prod").serialize()
            })
            .done(function(res) {
                $("#div_productos").html(res);
            })
            .always(function() {
                $("#loader").removeClass("loading");
            });
        });
    });
})(jQuery, window);
</script>
@stop

@section('content')
<fieldset>
    <legend>Buscador</legend>
    <form id="form_buscar_prod">
		<input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="row">
            <div class="small-12 medium-1 columns"><label for="nombre">Nombre</label></div>
            <div class="small-12 medium-2 columns"><input type="text" name="nombre" id="nombre" /></div>
            <div class="small-12 medium-2 columns end">
                <button class="tiny button rojo" id="btnBuscarProd">Buscar</button>
            </div>
        </div>
    </form>
</fieldset>
<div id="div_productos">
@include('producto.lista', array("productos" => $productos))
</div>
@stop