@extends('master')

@section('js_header')
<script src="{{ asset('js/accounting.min.js') }}"></script>
<script>
    (function ($, _, $ac, window) {
        function adicionarRegistroItem() {
            var $html = $("#registro").html();
            var $o = $($.parseHTML($html));
            var $cnt = $("#registros > div.row").size();
            $o.attr("id", "registro" + $cnt + Math.floor((Math.random() * 1000) + 1));
            hookRegistro($o, true);
            $("#registros").append($o);
            return $o;
        }

        function hookRegistro($o, $nuevo) {
            var $txtCantidad = $o.find("input[data-id='cantidad']");
            var $txtUnitario = $o.find("input[data-id='unitario']");
            var $valorUnitario = $o.find("input[data-id='valorUnitario']");
            var $txtTotal = $o.find("input[data-id='total']");
            var $valorTotal = $o.find("input[data-id='valorTotal']");
            var $cbxProducto = $o.find("select[data-id='producto']");
            var $remover = $o.find("a[data-id='quitar']");            

            if ($nuevo === true) {
                $txtCantidad.val(1);
            }

            $cbxProducto.change(function () {
                $valorUnitario.val($cbxProducto.find('option:selected').attr('data-precio'));
                var $precio = calcularPrecio();

                $txtUnitario.val($ac.formatMoney($precio.unitario));
                $txtTotal.val($ac.formatMoney($precio.total));
                $valorTotal.val($precio.total);

                var $soloLectura = $cbxProducto.find('option:selected').attr('data-abierto') !== 'Y';
                if ($soloLectura === true) {
                    $txtUnitario.attr('readonly', 'readonly');
                } else {
                    $txtUnitario.removeAttr('readonly');
                }
            });

            $txtUnitario.change(function (e) {
                var $precio = parseFloat($(this).val().replace("$", "").replace(".", "")); //$cbxProducto.find('option:selected').attr('data-precio');

                $valorUnitario.val($precio);
                $precio = calcularPrecio($precio);
                $(this).val($ac.formatMoney($precio.unitario));
                $txtTotal.val($ac.formatMoney($precio.total));
                $valorTotal.val($precio.total);
            });

            $txtCantidad.change(function () {
                var $precio = calcularPrecio();
                $txtTotal.val($ac.formatMoney($precio.total));
                $valorTotal.val($precio.total);
            });

            $remover.click(function (e) {
                e.preventDefault();
                if ($o.attr("id") !== "registro") {
                    $o.remove();
                }
            });

            var calcularPrecio = function ($_precio) {
                var $precio = parseFloat($valorUnitario.val());
                var $ret = {total: 0, unitario: 0};

                if ($precio > 0) {
                    $ret.unitario = $precio;
                    $ret.total = (parseInt($txtCantidad.val()) * $precio);
                }

                return $ret;
            };
        }
    
        $(document).ready(function () {
            var niveles = ["Bajo", "Medio", "Alto"];

            $("#btnBuscarCliente").click(function (e) {
                e.preventDefault();
                $("#loader").addClass("loading");
                $.ajax({
                    url: '{{ url("/clientes/buscar") }}',
                    method: 'post',
                    data: {'cedula': $("#cedula").val(), _token: '{!! csrf_token() !!}'},
                    dataType: 'json',
                    success: function (json) {
                        if (!_.isEmpty(json)) {
                            $("#nombre").val(json.nombres);
                            $("#apellidos").val(json.apellidos);
                            $("#direccion").val(json.direccion);
                            $("#telefono").val(json.telefono);

                            $("#id_vehiculo").html('');
                            $("#id_vehiculo").append('<option id=""></option>');
                            $.each(json.vehiculos, function (i, o) {
                                $("#id_vehiculo").append('<option value="' + o.id + '">' + o.nombre + ' - ' + o.placa + '</option>');
                            });

                            //$jsonDocumento.cedulaCliente =  $("#cedula").val();
                            //enviarSesion();
                        } else {
                            alert('Cliente no encontrado');
                        }

                        $("#loader").removeClass("loading");
                    }
                });
            });

            $("#id_vehiculo").change(function (e) {
                e.preventDefault();
                $("#loader").addClass("loading");
                $.ajax({
                    url: '{{ url("/orden/servicio/buscarVehiculo") }}/' + $(this).val(),
                    method: 'post',
                    data: {_token: '{!! csrf_token() !!}'},
                    dataType: 'json',
                    success: function (json) {
                        if (!_.isEmpty(json)) {
                            $("#marca").val(json.marca);
                            $("#motor").val(json.motor);
                            $("#serie").val(json.serie);
                            $("#color").val(json.color);
                            $("#placa").val(json.placa);
                            $("#km").val(json.km);
                            $("#cilindraje").val(json.cilindraje);
                            $("#caja").val(json.caja);
                        } else {
                            alert('Vehículo no encontrado');
                        }

                        $("#loader").removeClass("loading");
                    }
                });
            });
            
            @if(sizeof($documento->docDiagnostico))
            $("#id_vehiculo").change();           
            @endif
            
            $ac.settings = {
                currency: {
                    symbol: "$", // default currency symbol is '$'
                    format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
                    decimal: ",", // decimal point separator
                    thousand: ".", // thousands separator
                    precision: 0   // decimal places
                },
                number: {
                    precision: 0, // default precision on numbers is 0
                    thousand: ".",
                    decimal: ","
                }
            };

            $("#btnAdicionarReg").click(function (e) {
                e.preventDefault();
                adicionarRegistroItem();
            });

            $("#btnGuardarDocumento").click(function (e) {
                e.preventDefault();
                if ($("#cedula").val() === '') {
                    alert('Debe ingresar la cédula del cliente');
                    return;
                }

                if ($("#nombre").val() === '') {
                    alert('Debe ingresar el nombre del cliente');
                    return;
                }

                if ($("#apellidos").val() === '') {
                    alert('Debe ingresar los apellidos del cliente');
                    return;
                }

                if ($("#id_vehiculo").val() === '') {
                    alert('Debe seleccionar el vehículo');
                    return;
                }

                if ($("#marca").val() === '') {
                    alert('Debe ingresar la marca del vehículo');
                    return;
                }

                if ($("#motor").val() === '') {
                    alert('Debe ingresar el tipo de motor del vehículo');
                    return;
                }

                if ($("#color").val() === '') {
                    alert('Debe ingresar el color del vehículo');
                    return;
                }

                if ($("#serie").val() === '') {
                    alert('Debe ingresar la serie del vehículo');
                    return;
                }

                if ($("#placa").val() === '') {
                    alert('Debe ingresar la placa del vehículo');
                    return;
                }

                if ($("#km").val() === '') {
                    alert('Debe ingresar el kilometraje del vehículo');
                    return;
                }

                if ($("#cilindraje").val() === '') {
                    alert('Debe ingresar el cilindraje del vehículo');
                    return;
                }

                if ($("#caja").val() === '') {
                    alert('Debe ingresar el tipo de caja del vehículo');
                    return;
                }

                if ($("#observaciones").val() === '') {
                    if (!confirm('No ha ingresado las observaciones, ¿desea continuar?')) {
                        return;
                    }
                }

                if ($("#estado_general").val() === '') {
                    if (!confirm('No ha ingresado el estado general del vehículo, ¿desea continuar?')) {
                        return;
                    }
                }

                if (!confirm('¿Está seguro de terminar este documento?')) {
                    return;
                }

                $("#loader").addClass("loading");

                var $form = new FormData($('#form_orden')[0]);
                $.ajax({
                    url: '{{ url("orden/servicio/guardar") }}',
                    data: $form,
                    method: 'post',
                    cache: false,
                    contentType: false,
                    processData: false
                })
                .done(function (res) {
                    alert('La orden de servicio ha sido guardada exitosamente');
                    window.location.reload();
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseJSON.error.message);
                })
                .always(function () {
                    $("#loader").removeClass("loading");
                });
            });
            
            hookRegistro($("#registro"), false);
            
            $("div[rel='registro']").each(function (i, o) {
                hookRegistro($(o), false);
            });
        });
    })(jQuery, window._, accounting, window);
</script>
@stop

@section('content')
<h2>Realizar una orden de servicio</h2>
<form id="form_orden" name="form_orden" action="{{ url('orden/servicio/guardar') }}" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" name="id" id="id" value="{{ $documento->id }}" />
    <input type="hidden" name="id_docdg" id="id_docdg" value="@if(sizeof($documento->docDiagnostico)){{ $documento->docDiagnostico[0]->id }}@endif" />
    
    <div id="datos_mapa"></div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Tipo</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select id="id_tipo" name="id_tipo">
                @foreach($tipos as $t)
                <option value="{{ $t->id }}" @if($t->id == $documento->id_tipo) selected @endif>{{ $t->sigla }} - {{ $t->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label for="estado">Documento cerrado
                    <input type="checkbox" name="estado" id="estado" value="C" @if(sizeof($documento->docDiagnostico) && $documento->docDiagnostico[0]->estado == "C") checked="checked" @endif />
                </label>
            </div>
        </div>
    </div>

    <fieldset>
        <legend>Datos de veh&iacute;culo</legend>
        <div class="row">
            <div class="small-12 medium-2 columns"><label for="cedula">C&eacute;dula</label></div>
            <div class="small-12 medium-4 columns">
                <div class="row collapse">
                    <div class="small-9 columns"><input type="text" id="cedula" name="cedula" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->id_nit }}" @endif/></div>
                    <div class="small-3 columns"><button class="button default rojo tiny" id="btnBuscarCliente">Buscar</button></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="nombre">Nombre propietario</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="nombre" name="nombre" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->nombres }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="apellido">Apellido propietario</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="apellidos" name="apellidos" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->apellidos }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="telefono">Tel&eacute;fono</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="telefono" name="telefono" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->telefono }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="direccion">Direcci&oacute;n</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="direccion" name="direccion" @if(sizeof($documento->cliente)) value="{{ $documento->cliente->direccion }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="vehiculo">Veh&iacute;culo</label></div>
            <div class="small-12 medium-4 columns end">
                <select id="id_vehiculo" name="id_vehiculo">
                    @if(sizeof($documento->cliente) && sizeof($documento->cliente->vehiculos))
                    
                    @foreach($documento->cliente->vehiculos as $v)
                    <option value="{{ $v->id }}" @if($v->id == $documento->id_vehiculo) selected @endif>{{ $v->placa }} - {{ $v->nombre }}</option>
                    @endforeach
                    
                    @endif
                </select>
            </div>
            <div class="small-12 medium-2 columns"><label for="marca">Marca</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="marca" name="marca" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->marca }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="motor">Motor</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="motor" name="motor" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->motor }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="serie">Serie</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="serie" name="serie" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->serie }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="color">Color</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="color" name="color" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->color }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="placa">Placa</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="placa" name="placa" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->placa }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="km">Km</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="km" name="km" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->km }}" @endif/></div>
            <div class="small-12 medium-2 columns"><label for="cilindraje">Cil. motor</label></div>
            <div class="small-12 medium-4 columns"><input type="text" id="cilindraje" name="cilindraje" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->cilindraje }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="caja">Caja</label></div>
            <div class="small-12 medium-4 columns end"><input type="text" id="caja" name="caja" @if(sizeof($documento->vehiculo)) value="{{ $documento->vehiculo->caja }}" @endif/></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="observaciones">Observaciones</label></div>
            <div class="small-12 medium-10 columns"><textarea id="observaciones" name="observaciones">{{ $documento->observaciones }}</textarea></div>
            <div class="clearfix"></div>
            <div class="small-12 medium-2 columns"><label for="estado_general">Estado general</label></div>
            <div class="small-12 medium-10 columns"><input type="text" id="estado_general" name="estado_general" value="@if(sizeof($documento->docDiagnostico)){{ $documento->docDiagnostico[0]->estado_general }}@endif" /></div>
        </div>
    </fieldset>
    
    <ul class="tabs" data-tab>
        <li class="tab-title active"><a href="#diagnosticos">Inventario y revisi&oacute;n</a></li>
        <li class="tab-title"><a href="#productos">Productos y servicios</a></li>
    </ul>
    
    <div class="tabs-content">
        <div class="content active" id="diagnosticos">
            @foreach($categorias as $c)
            <fieldset>
                <legend>{{ $c->nombre }}</legend>
                <div class="row">
                    <div class="small-4 columns"><strong>Revisi&oacute;n de</strong></div>
                    <div class="small-2 columns"><strong>Cnt</strong></div>
                    <div class="small-4 columns text-center">
                        <strong>Estado</strong>
                        <div class="row">
                            @foreach($opciones as $o)
                            <div class="small-3 columns">{{ $o->nombre }}</div>
                            @endforeach
                        </div>
                    </div>
                    <div class="small-2 columns"><strong>Observaciones</strong></div>
                </div>
                <hr />
                @foreach($c->items as $i)
                <div class="row separador_gris">
                    <div class="small-4 columns">
                        {{ $i->nombre }}
                    </div>
                    <div class="small-2 columns">
                        @if ($i->requiere_cant == "Y")
                        <div class="row">
                            <div class="small-8 columns"><input type="text" name="cant_item[{{ $i->id }}]" id="cant_item_{{ $i->id }}" class="caja reducida" @if(sizeof($i->diagnosticos)) value="{{ $i->diagnosticos[0]->cantidad }}" @endif/></div>
                            <div class="small-4 columns"><span>{{ $i->unm }}</span></div>
                        </div>
                        @else
                        &nbsp;
                        @endif
                    </div>
                    <div class="small-4 columns text-center">
                        <div class="row">
                            @foreach($opciones as $o)
                            <div class="small-3 columns"><input type="radio" id="resp_item_{{ $i->id }}_opc{{ $o->id }}" name="resp[{{ $i->id }}]" value="{{ $o->id }}" @if(sizeof($i->diagnosticos) && $i->diagnosticos[0]->id_opc_dg == $o->id) checked @endif /></div>
                            @endforeach
                        </div>
                    </div>
                    <div class="small-2 columns"><input type="text" name="observaciones_item[{{ $i->id }}]" id="observaciones_item_{{ $i->id }}" class="caja reducida" @if(sizeof($i->diagnosticos)) value="{{ $i->diagnosticos[0]->observaciones }}" @endif /></div>
                </div>
                @endforeach
            </fieldset>
            @endforeach
            
            <div class="small-12 columns">
                <big>Recomendaciones del taller</big>
                <textarea id="recomendaciones" name="recomendaciones" rows="5">@if(sizeof($documento->docDiagnostico)){{ $documento->docDiagnostico[0]->recomendaciones }}@endif</textarea>
            </div>
        </div>
        
        <div class="content" id="productos">
            <fieldset>
                <legend>Productos y servicios</legend>
                <div class="row">
                    <div class="medium-3 small-12 columns">
                        <strong>Producto</strong>
                    </div>
                    <div class="medium-2 small-12 columns text-right">
                        <strong>Cantidad</strong>
                    </div>
                    <div class="medium-2 small-12 columns text-right">
                        <strong>Vlr. Unitario</strong>
                    </div>
                    <div class="medium-2 small-12 columns text-right end">
                        <strong>Total</strong>
                    </div>
                    <hr />
                </div>
                <div id="registro">
                    <div class="row" rel="registro">
                        <div class="medium-3 small-12 columns">
                            <select data-id="producto" name="id_producto[]">
                                <option value="">Seleccione un producto</option>
                                @foreach($productos as $p)
                                <option value="{{ $p->id }}" data-precio="{{ $p->precio }}" data-abierto="{{ $p->precio_abierto }}" data-tipo="{{ $p->tipo }}">{{ $p->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="medium-2 small-12 columns">
                            <input type="text" name="cantidad[]" data-id="cantidad" value="1" class="text-right" />
                        </div>
                        <div class="medium-2 small-12 columns">
                            <input type="text" name="unitario[]" data-id="unitario" value="0" readonly="readonly" class="text-right" />
                            <input type="hidden" name="valor_unitario[]" data-id="valorUnitario" value="0" />
                        </div>
                        <div class="medium-2 small-11 columns">
                            <input type="text" name="total[]" data-id="total" value="0" readonly="readonly" class="text-right" />
                            <input type="hidden" name="valor_total[]" data-id="valorTotal" value="0" />
                        </div>
                        <div class="small-2 columns">
                            <a href="#" data-id="quitar" title="Quitar producto"><i class="fi-x-circle">&nbsp;</i></a>
                        </div>
                    </div>
                </div>
                <div id="registros">
                    @if(sizeof($documento->docDiagnostico) && sizeof($documento->docDiagnostico[0]->productos))
                    
                    @foreach($documento->docDiagnostico[0]->productos as $pd)
                    <div class="row" rel="registro">
                        <div class="medium-3 small-12 columns">
                            <select data-id="producto" name="id_producto[]">
                                <option>Seleccione un producto</option>
                                @foreach($productos as $p)
                                <option value="{{ $p->id }}" data-precio="{{ $p->precio }}" data-abierto="{{ $p->precio_abierto }}" data-tipo="{{ $p->tipo }}" @if($pd->id_producto == $p->id) selected @endif>{{ $p->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="medium-2 small-12 columns">
                                <input type="text" name="cantidad[]" data-id="cantidad" value="{{ $pd->cantidad }}" class="text-right" />
                        </div>
                        <div class="medium-2 small-12 columns">
                            <input type="text" name="unitario[]" data-id="unitario" value="{{ $pd->valor_unitario }}" readonly="readonly" class="text-right" />
                            <input type="hidden" name="valor_unitario[]" data-id="valorUnitario" value="{{ $pd->valor_unitario }}" />
                        </div>
                        <div class="medium-2 small-11 columns">
                            <input type="text" name="total[]" data-id="total" value="{{ $pd->valor_total }}" readonly="readonly" class="text-right" />
                            <input type="hidden" name="valor_total[]" data-id="valorTotal" value="{{ $pd->valor_total }}" />
                        </div>
                        <div class="small-2 columns">
                            <a href="#" data-id="quitar" title="Quitar producto"><i class="fi-x-circle">&nbsp;</i></a>
                        </div>
                    </div>
                    @endforeach
                    
                    @endif
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <a class="small button rojo right" href="#" id="btnAdicionarReg" />A&ntilde;adir registro</a>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns text-right">
            <a class="button gris" href="{{ url('/orden/servicio/') }}" />Cancelar</a>
            <input type="button" value="Guardar" class="button naranja" id="btnGuardarDocumento" />
        </div>
    </div>
</form>
@stop