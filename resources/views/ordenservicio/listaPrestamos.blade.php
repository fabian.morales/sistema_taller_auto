<h2>Pr&eacute;stamos {{ $documento->tipoDocumento->nombre }} {{ $documento->tipoDocumento->sigla }} - {{ $documento->num }}</h2>
<div class="row item lista">
    <div class="small-1 columns">N&uacute;m</div>
    <div class="small-4 columns">Empleado</div>
    <div class="small-4 columns">Fecha</div>
    <div class="small-3 columns">Valor</div>
</div>

@forelse($documento->docDiagnostico[0]->prestamos as $p)
<div class="row item lista">
    <div class="small-1 columns">{{ $p->id }}</div>
    <div class="small-4 columns">{{ $p->empleado->nombre }}</div>
    <div class="small-4 columns">{{ $p->fecha }}</div>
    <div class="small-3 columns">{{ $p->valor }}</div>
</div>
@empty
    <p class="text-center">No se encontr&oacute; prestamos</p>
@endforelse