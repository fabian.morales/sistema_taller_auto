<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('orden/servicio/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">&Uacute;ltimas ordenes de servicio</div>
</div>
<div class="row item lista">
    <div class="small-3 columns">N&uacute;m</div>
    <div class="small-4 columns">Tipo</div>
    <div class="small-3 columns">Fecha</div>
    <div class="small-2 columns">Acciones</div>
</div>

@forelse($ordenes as $o)
<div class="row item lista">
    <div class="small-3 columns">{{ $o->num }}</div>
    <div class="small-4 columns">{{ $o->tipoDocumento->sigla }}</div>
    <div class="small-3 columns">{{ $o->fecha }}</div>
    <div class="small-2 columns">
        <a href="{{ url('/orden/servicio/editar/'.$o->id) }}" title="Editar documento"><i class="fi-pencil">&nbsp;</i></a>
        <a href="{{ url('/orden/servicio/detalle/'.$o->id) }}"><i class="fi-magnifying-glass">&nbsp;</i></a>
        <a href="{{ url('/orden/servicio/pdf/'.$o->id) }}" target='_blank' title="Ver en PDF"><i class="fi-page-pdf"></i>&nbsp;</a>
        <a href="{{ url('/orden/servicio/facturar/'.$o->id) }}" title="Generar cuenta de cobro"><i class="fi-shopping-bag"></i>&nbsp;</a>
        <a href="{{ url('/orden/servicio/prestamo/'.$o->id) }}" title="Registrar pr&eacute;stamo" data-featherlight><i class="fi-dollar"></i>&nbsp;</a>
        <a href="{{ url('/orden/servicio/prestamo/lista/'.$o->id) }}" title="Lista de pr&eacute;stamos" data-featherlight><i class="fi-clipboard-notes"></i>&nbsp;</a>

    </div>
</div>
@empty
    <p class="text-center">No se encontr&oacute; documentos</p>
@endforelse

<div class="row">
    <div class="small-12 columns text-center">
        {!! $ordenes->render() !!}
    </div>
</div>