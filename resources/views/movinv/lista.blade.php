<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('movimientos/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">&Uacute;ltimos movimientos</div>
</div>        
<div class="row item lista">
    <div class="small-3 columns">N&uacute;m</div>
    <div class="small-4 columns">Tipo</div>
    <div class="small-3 columns">Fecha</div>            
    <div class="small-2 columns">Ver</div>            
</div>

@forelse($movimientos as $m)
<div class="row item lista">
    <div class="small-3 columns">{{ $m->num }}</div>
    <div class="small-4 columns">{{ $m->tipoDocumento->sigla }}</div>
    <div class="small-3 columns">{{ $m->fecha }}</div>            
    <div class="small-2 columns"><a href="{{ url('/movimientos/detalle/'.$m->id) }}"><i class="fi-magnifying-glass">&nbsp;</i></a></div>            
</div>
@empty
    <p class="text-center">No se encontr&oacute; documentos</p>
@endforelse

<div class="row">
    <div class="small-12 columns text-center">
        {!! $movimientos->render() !!}
    </div>
</div>