@extends('masterPdf')

@section('content')
    <table cellspacing="5" cellpadding="0" class="sep">
        <tr>
            <td><strong>Recibo de pago</strong></td>
            <td>Periodo {{ $periodo->codigo }}</td>
        </tr>
        <tr>
            <td><strong>Fecha:</strong></td>
            <td>{{ $periodo->fecha_liq }}</td>
        </tr>
        <tr>
            <td><strong>Empleado:</strong></td>
            <td>{{ $empleado->cedula }} - {{ $empleado->nombre }}</td>
        </tr>       
    </table>
    
    <h2>Liquidación de servicios</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr class='doble'>
                <td><strong>Fecha</strong></td>
                <td><strong>Documento</strong></td>
                <td><strong>Servicio</strong></td>
                <td><strong>Cargo</strong></td>
                <td class="right"><strong>Valor</strong></td>
            </tr>

            @foreach($periodo->detalles as $d)
            
            @if($d->documento->tipoDocumento->tipo_mov == "F")
            <tr>
                <td>{{ $d->documento->fecha }}</td>
                <td>{{ $d->documento->tipoDocumento->sigla }} - {{ $d->documento->num }}</td>
                <td>{{ $d->servicio->nombre }}</td>
                <td>{{ $d->cargo->nombre }}</td>
                <td class="right">$ {{ $d->valor }}</td>
            </tr>
            
            @endif
            
            @endforeach
        </table>
    </div>
    
    <h2>Pr&eacute;stamos</h2>
    <div class='bordes sep'>
        <table cellspacing="0" cellpadding="0">
            <tr class='doble'>
                <td><strong>Fecha</strong></td>
                <td><strong>Documento</strong></td>               
                <td class="right"><strong>Valor</strong></td>
            </tr>

            @foreach($periodo->detalles as $d)
            
            @if($d->documento->tipoDocumento->tipo_mov == "S")
            <tr>
                <td>{{ $d->documento->fecha }}</td>
                <td>{{ $d->documento->tipoDocumento->sigla }} - {{ $d->documento->num }}</td>
                <td class="right">$ {{ $d->valor }}</td>
            </tr>
            
            @endif
            
            @endforeach
        </table>
    </div>
    
    <h2>Total devengado: ${{ $periodo->resumenes[0]->valor }}</h2>
@stop