@extends('master')

@section('content')
<div class="row titulo lista">
    <div class="small-12 columns">Detalle del periodo</div>
</div> 
<fieldset>
    <legend>Periodo</legend>
    <div class="row">
        <div class="small-3 columns"><strong>Codigo: </strong></div>
        <div class="small-3 columns">{{ $periodo->codigo }}</div>
        <div class="small-3 columns"><strong>Fecha liquidaci&oacute;n: </strong></div>
        <div class="small-3 columns">{{ $periodo->fecha_liq }}</div>
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Fecha inicio: </strong></div>
        <div class="small-3 columns">{{ $periodo->fecha_inicio }}</div>
        <div class="small-3 columns"><strong>Fecha fin: </strong></div>
        <div class="small-3 columns">{{ $periodo->fecha_fin }}</div>
        <div class="clearfix"></div>
    </div>
</fieldset>

<fieldset>
    <legend>Empleados</legend>
    <div class="row item lista">
        <div class="small-5 columns"><strong>Nombre</strong></div>
        <div class="small-4 columns"><strong>Valor</strong></div>
        <div class="small-3 columns"><strong>Recibo</strong></div>
    </div>

    @foreach($periodo->resumenes as $r)
    <div class="row item lista">
        <div class="small-5 columns">{{ $r->empleado->nombre }}</div>            
        <div class="small-4 columns">${{ $r->valor }}</div>
        <div class="small-3 columns"><a href="{{ url('/nomina/recibo/'.$periodo->id.'/'.$r->id_empleado) }}" target='_blank'><i class="fi-page-pdf"></i>&nbsp;Ver en PDF</a></div>
    </div>
    
    @endforeach
</fieldset>

<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('/nomina/periodo') }}" class="button">Regresar</a>
    </div>
</div>
@stop
