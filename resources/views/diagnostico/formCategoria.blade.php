@extends('master')

@section('content')
<h2>Datos de la categor&iacute;a de &iacute;tems de diagn&oacute;stico</h2>
<form id="form_dg_categoria" name="form_dg_categoria" action="{{ url('/diagnostico/categoria/guardar') }}" method="post">
    <input type="hidden" id="id" name="id" value="{{ $categoria->id }}" />
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="nombre" id="nombre" value="{{ $categoria->nombre }}" />
        </div>
    </div>    
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/diagnostico/categoria/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />                    
        </div>
    </div>              
</form>
@stop